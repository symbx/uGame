//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_GAMESTATELOADING_H
#define UGAME_GAMESTATELOADING_H

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Thread.hpp>
#include "GameState.h"
#include "../core/Application.h"

class GameStateLoading : public GameState {
public:
    GameStateLoading();
    virtual void init(Application* app);
    virtual void clean();
    virtual void pause();
    virtual void resume();
    virtual void event(sf::Event& event);
    virtual void update(float time);
    virtual void draw(sf::RenderWindow* render);

private:
    Application* _app;
    sf::RectangleShape _bg;
    sf::RectangleShape _progress;
    sf::Thread* _work;
    unsigned int _width;
    unsigned int _height;
};


#endif //UGAME_GAMESTATELOADING_H
