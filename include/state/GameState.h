//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_GAMESTATE_H
#define UGAME_GAMESTATE_H

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class Application;

class GameState {
public:
    virtual void init(Application* app) = 0;
    virtual void clean() = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual void event(sf::Event& event) = 0;
    virtual void update(float time) = 0;
    virtual void draw(sf::RenderWindow* render) = 0;
};


#endif //UGAME_GAMESTATE_H
