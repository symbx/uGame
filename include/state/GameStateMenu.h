//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_GAMESTATEMENU_H
#define UGAME_GAMESTATEMENU_H

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include "GameState.h"
#include "../core/Application.h"
#include "../gui/BigButton.h"
#include "../gui/BigProgressBar.h"
#include "../gui/Button.h"

class GameStateMenu : public GameState {
public:
    GameStateMenu();
    virtual void init(Application* app);
    virtual void clean();
    virtual void pause();
    virtual void resume();
    virtual void event(sf::Event& event);
    virtual void update(float time);
    virtual void draw(sf::RenderWindow* render);

private:
    Application* _app;
    sf::VertexArray _vertex;
    sf::Texture _bg;
    BigButton* _testBigBtn1;
    BigButton* _testBigBtn2;
    BigProgressBar* _progressBar;
    float _progress;
    Button* _testBtn;
};


#endif //UGAME_GAMESTATEMENU_H
