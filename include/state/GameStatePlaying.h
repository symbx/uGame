//
// Created by symbx on 23.03.17.
//

#ifndef UGAME_GAMESTATEPLAYING_H
#define UGAME_GAMESTATEPLAYING_H

#include "../core/Application.h"
#include "GameState.h"
#include "../game/Chunk.h"

class GameStatePlaying : public GameState {
public:
    GameStatePlaying();
    virtual void init(Application* app);
    virtual void clean();
    virtual void pause();
    virtual void resume();
    virtual void event(sf::Event& event);
    virtual void update(float time);
    virtual void draw(sf::RenderWindow* render);

private:
    Application* _app;
    Chunk* _chunk;
};


#endif //UGAME_GAMESTATEPLAYING_H
