//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_LOGGER_H
#define UGAME_LOGGER_H

#include <string>
#include <fstream>

#define LOG Logger::log

class Logger {
public:
    enum LoggerSeverity {
        SeverityInfo,
        SeverityDebug,
        SeverityWarning,
        SeverityError
    };

    static void init(const std::string& appDir);
    static void clean();
    static void log(const std::string& msg, LoggerSeverity severity);
    static void log(const char* msg, LoggerSeverity severity);
    static void log(const std::string& msg);
    static void log(const char* msg);

private:
    static std::string _getTime();
    static std::ofstream* _file;
    static time_t _rawTime;
    static struct tm* _timeInfo;
};


#endif //UGAME_LOGGER_H
