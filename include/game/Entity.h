//
// Created by symbx on 28.03.17.
//

#ifndef UGAME_ENTITY_H
#define UGAME_ENTITY_H


#include <SFML/Graphics/Drawable.hpp>
#include <set>
#include <queue>
#include "../../include/core/ResourceManager.h"

class Entity : public sf::Drawable {
public:
    enum Direction {
        DOWN, LEFT, RIGHT, UP
    };
    Entity(ResourceManager* res, sf::Uint32 id);
    virtual sf::Uint32 getID();
    virtual void setDirection(Direction d);
    virtual Direction getDirection();
    virtual void update(float time) = 0;
    virtual void setMap(std::set<Entity*>* entitiesMap);
    virtual void goTo(sf::Uint8 x, sf::Uint8 y);
    virtual void resetPath();
    virtual void setPos(sf::Uint8 x, sf::Uint8 y);
    virtual sf::Vector2u getPos();

protected:
    sf::Transform _transform;
    sf::Uint32 _ID;
    Direction _dir;
    ResourceManager* _res;
    sf::Uint8 _posX;
    sf::Uint8 _posY;
    std::queue<short> _path; //y*32+x
    float _shiftX; // -16 - +16
    float _shiftY; // -16 - +16
    std::set<Entity*>* _entitiesMap;
};


#endif //UGAME_ENTITY_H
