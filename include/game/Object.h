//
// Created by symbx on 25.03.17.
//

#ifndef UGAME_OBJECT_H
#define UGAME_OBJECT_H

#include <SFML/Config.hpp>

struct Object {
    struct ObjectState* States;
    sf::Uint8 StatesCount;
    sf::Uint8 Image;
    sf::Uint8 Width;
    sf::Uint8 Height;
};
struct ObjectState {
    struct ObjectFragment* Fragments;
    sf::Uint8 FragmentsCount;
    sf::Int8 InteractX;
    sf::Int8 InteractY;
};
struct ObjectFragment {
    float FrameTime;
    sf::Uint16* Frames;
    sf::Uint8 FramesCount;
    //RAW SIZE NOT NEED TO MULTIPLY 32
    sf::Uint8 PosX;
    sf::Uint8 PosY;
    sf::Uint8 Width;
    sf::Uint8 Height;
};

#endif //UGAME_OBJECT_H
