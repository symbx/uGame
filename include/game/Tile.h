//
// Created by symbx on 23.03.17.
//

#ifndef UGAME_TILE_H
#define UGAME_TILE_H

#include <SFML/Config.hpp>

struct Tile {
    float frameTime;
    sf::Uint16* frames;
    sf::Uint8 framesCount;
};

#endif //UGAME_TILE_H
