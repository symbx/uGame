//
// Created by symbx on 23.03.17.
//

#ifndef UGAME_CHUNK_H
#define UGAME_CHUNK_H


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <set>
#include "../core/Application.h"
#include "Tile.h"
#include "TileBlock.h"
#include "Object.h"
#include "ObjectBlock.h"
#include "Entity.h"
#include "../Constants.h"

class Chunk : public sf::Drawable {
public:
    Chunk(Application* app, int world, int layer, int x, int y);
    ~Chunk();
    void update(float time);
    void event(sf::Event& event);
    void loadTestEntity();
    void load(int world, int layer, int x, int y);

private:
    Application* _app;
    std::vector<sf::Texture> _images;
    std::vector<Tile*> _tiles;
    TileBlock* _tilesMap[MAP_LAYER_TILES];
    bool _solidMap[MAP_LAYER_TILES];
    sf::View _view;
    std::vector<sf::Vertex> _vertex;
    std::vector<Object*> _objects;
    ObjectBlock* _objectMap[MAP_TILES_COUNT];
    std::map<sf::Uint32, Entity*> _entities;
    std::set<Entity*> _entitiesMap[MAP_TILES_COUNT];
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    sf::FloatRect _calcView(int w, int h);
    sf::Texture _cursorTex[4]; //Go, No, Use, Say
    sf::Sprite _cursor;
};


#endif //UGAME_CHUNK_H
