//
// Created by symbx on 28.03.17.
//

#ifndef UGAME_ENTITYPLAYER_H
#define UGAME_ENTITYPLAYER_H


#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "Entity.h"
#include "../../include/core/ResourceManager.h"

class EntityPlayer : public Entity {
public:
    enum Fraction {
        Elf,
        Human,
        DarkElf,
        UnDead
    };
    enum Class {
        Druid,
        Priest,
        Reynger,
        Rogue,
        Paladin,
        Mage
    };
    enum Action {
        None,
        Walk,
        AttackSpear,
        AttackBow,
        AttackSword1H,
        CastShield,
        Die
    };
    enum Gender {
        Male,
        Female
    };
    EntityPlayer(ResourceManager* res, sf::Uint32 id, Gender g, Fraction f, Class c);
    void setAction(Action a);
    virtual void update(float time);
    //virtual void goTo(std::set<Entity*>* entitiesMap, sf::Uint8 x, sf::Uint8 y);
    virtual void setDirection(Direction d);
protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void _loadTextures();
    void _updateFrame();
    Gender _gender;
    Fraction _fraction;
    Class _class;
    Action _action;
    int _actionY;
    int _dirY;
    sf::Uint8 _frame;
    sf::Uint8 _maxFrames;
    float _frameTime;
    sf::Texture _bodyTexture;
    sf::VertexArray _body;
};


#endif //UGAME_ENTITYPLAYER_H
