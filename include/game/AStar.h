//
// Created by symbx on 04.05.17.
//

#ifndef UGAME_ASTAR_H
#define UGAME_ASTAR_H


#include <SFML/Config.hpp>
#include <cmath>

class AStar {
    struct PathNode {
        sf::Int8 x;
        sf::Int8 y;
        //TODO: May be better int?
        float start;
        float finish;
        float cost;

        bool operator<(const PathNode&rhs) const {
            return cost > rhs.cost;
        };

        float estimate(const sf::Int8 dx, const sf::Int8 dy) {
            sf::Int8 tx = dx-x;
            sf::Int8 ty = dy-y;
            cost = (float) sqrt(tx * tx + ty * ty);
            return cost;
        }
    };

public:
    static std::vector<short> GetPath(sf::Int8 sX, sf::Int8 sY, sf::Int8 eX, sf::Int8 eY, bool* map);

};


#endif //UGAME_ASTAR_H
