//
// Created by symbx on 25.03.17.
//

#ifndef UGAME_OBJECTBLOCK_H
#define UGAME_OBJECTBLOCK_H

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include "Object.h"

class ObjectBlock : public sf::Drawable {
public:
    struct FragmentData {
        float Time;
        sf::Uint8 Frame;
    };
    ObjectBlock(Object* obj, sf::Texture* texture, sf::Uint8 x, sf::Uint8 y);
    ~ObjectBlock();
    void setState(sf::Uint8 state);
    void update(float time);

private:
    Object* _object;
    sf::Uint8 _state;
    FragmentData* _frames;
    sf::Texture* _texture;
    sf::VertexArray _vertex;
    sf::Transform _transform;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void _updateFragment(int i);
};


#endif //UGAME_OBJECTBLOCK_H
