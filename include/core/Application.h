//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_APPLICATION_H
#define UGAME_APPLICATION_H

#include <string>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include "Config.h"
#include "ResourceManager.h"
#include "GameStateManager.h"

class Application {
public:
    Application(const std::string& appDir);
    ~Application();
    //void args(int argc, char* argv[]);
    void run();
    void quit();
    ResourceManager* getResourceManager();
    GameStateManager* getStateManager();
    sf::RenderWindow* getWindow();

private:
    void _loop();
    bool _running;
    sf::Clock _clock;
#ifdef DEBUG
    sf::Text _fpsText;
    sf::Font _fpsFont;
    void _calcFPS(float time);
#endif
    Config* _cfg;
    sf::RenderWindow* _window;
    ResourceManager* _resources;
    GameStateManager* _states;
};


#endif //UGAME_APPLICATION_H
