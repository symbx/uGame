//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_CONFIG_H
#define UGAME_CONFIG_H

#include <string>
#include <unordered_map>

class Config {
public:
    Config(const std::string& appDir);
    ~Config();
    std::string get(std::string& k);
    void set(std::string& k, std::string& v);
    void save();

private:
    std::string _cfg;
    std::unordered_map<std::string, std::string> _map;
};


#endif //UGAME_CONFIG_H
