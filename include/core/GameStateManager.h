//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_GAMESTATEMANAGER_H
#define UGAME_GAMESTATEMANAGER_H


#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "../state/GameState.h"

class Application;

class GameStateManager {
public:
    GameStateManager(Application* app);
    ~GameStateManager();
    void init();
    void open(GameState* state);
    void close();
    GameState* current();
    void event(sf::Event& event);
    void update(float time);
    void draw(sf::RenderWindow* render);

private:
    Application* _app;
    std::vector<GameState*> _states;
};


#endif //UGAME_GAMESTATEMANAGER_H
