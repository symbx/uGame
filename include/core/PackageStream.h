//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_PACKAGESTREAM_H
#define UGAME_PACKAGESTREAM_H

#include <string>
#include <fstream>
#include <SFML/System/InputStream.hpp>

class PackageStream : public sf::InputStream {
public:
    PackageStream(const std::string& file, sf::Int64 offset, sf::Int64 size);
    virtual ~PackageStream();
    virtual sf::Int64 read(void* data, sf::Int64 size);
    virtual sf::Int64 seek(sf::Int64 pos);
    virtual sf::Int64 tell();
    virtual sf::Int64 getSize();

protected:
private:
    std::ifstream _stream;
    sf::Int64 _offset;
    sf::Int64 _size;
    sf::Int64 _pos;
};


#endif //UGAME_PACKAGESTREAM_H
