//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_RESOURCEMANAGER_H
#define UGAME_RESOURCEMANAGER_H

#include <string>
#include <vector>
#include <unordered_map>
#include <SFML/Config.hpp>
#include <SFML/System/InputStream.hpp>

class ResourceManager {
public:
    struct ResourceEntry
    {
        sf::Int64 offset;
        sf::Int64 size;
        std::string package;
    };
    ResourceManager(const std::string& appDir);
    ~ResourceManager();
    void listPackages();
    void readPackages();
    void reload();
    float getProgress();
    sf::InputStream* getStream(const std::string& f);
    sf::InputStream* getStream(const char* f);

private:
    bool _fileExists(const std::string& f);
    void _readPackage(const std::string& f);
    std::string _dir;
    std::vector<std::string> _packages;
    std::unordered_map<std::string, ResourceEntry> _entries;
    bool _complete;
    float _progressPerFile;
    float _progress;
};


#endif //UGAME_RESOURCEMANAGER_H
