//
// Created by symbx on 21.03.17.
//

#ifndef UGAME_THEME_H
#define UGAME_THEME_H

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/InputStream.hpp>
#include <SFML/Graphics/Text.hpp>
#include "../core/Application.h"

class Theme {
public:
    //Texture{Control}{State}{Part}
    enum TextureType {
        TextureEmpty,
        //Normal
        TextureBigBtnNLeft,
        TextureBigBtnNMid,
        TextureBigBtnNRight,
        //Hover
        TextureBigBtnHLeft,
        TextureBigBtnHMid,
        TextureBigBtnHRight,
        //Press
        TextureBigBtnPLeft,
        TextureBigBtnPMid,
        TextureBigBtnPRight,
        //Focus
        TextureBigBtnFLeft,
        TextureBigBtnFMid,
        TextureBigBtnFRight,
        //Disabled
        TextureBigBtnDLeft,
        TextureBigBtnDMid,
        TextureBigBtnDRight,
        TextureBigBtnOffset,
        //Disabled
        TextureBigProgressBarD,
        TextureBigProgressBgD,
        //Enabled
        TextureBigProgressBarE,
        TextureBigProgressBgE,
        TextureBigProgressOffset,
        //Disabled
        TextureProgressBarD,
        TextureProgressBgD,
        //Enabled
        TextureProgressBarE,
        TextureProgressBgE,
        TextureProgressOffset,
        //Normal
        TextureBtnN_TL,
        TextureBtnN_T,
        TextureBtnN_TR,
        TextureBtnN_R,
        TextureBtnN_BR,
        TextureBtnN_B,
        TextureBtnN_BL,
        TextureBtnN_L,
        TextureBtnN_BG,
        //Hover
        TextureBtnH_TL,
        TextureBtnH_T,
        TextureBtnH_TR,
        TextureBtnH_R,
        TextureBtnH_BR,
        TextureBtnH_B,
        TextureBtnH_BL,
        TextureBtnH_L,
        TextureBtnH_BG,
        //Press
        TextureBtnP_TL,
        TextureBtnP_T,
        TextureBtnP_TR,
        TextureBtnP_R,
        TextureBtnP_BR,
        TextureBtnP_B,
        TextureBtnP_BL,
        TextureBtnP_L,
        TextureBtnP_BG,
        //Focus
        TextureBtnF_TL,
        TextureBtnF_T,
        TextureBtnF_TR,
        TextureBtnF_R,
        TextureBtnF_BR,
        TextureBtnF_B,
        TextureBtnF_BL,
        TextureBtnF_L,
        TextureBtnF_BG,
        //Disabled
        TextureBtnD_TL,
        TextureBtnD_T,
        TextureBtnD_TR,
        TextureBtnD_R,
        TextureBtnD_BR,
        TextureBtnD_B,
        TextureBtnD_BL,
        TextureBtnD_L,
        TextureBtnD_BG,
        TextureBtnOffset,
        TexturesCount
    };
    enum TextStyleType {
        TextStyleEmpty,
        TextStyleBigBtnN,
        TextStylesCount
    };
    struct TextStyle {
        sf::Uint32 size;
        sf::Text::Style style;
        sf::Color color;
    };
    static bool loadSpriteSheet(sf::InputStream& img, sf::InputStream& sheet);
    static const sf::Texture& getSpriteSheet();
    static const sf::IntRect& getTextureRect(TextureType type);
    static bool loadFont(sf::InputStream& stream);
    static const sf::Font& getFont();
    static bool loadTextStyle(sf::InputStream& stream);
    static const TextStyle& getTextStyle(TextStyleType type);
    static void init(Application* app);
    static void clean();
private:
    static sf::Texture _spriteSheet;
    static sf::Font _font;
    static sf::InputStream* _fontStream;
    static sf::IntRect _rectangles[TexturesCount];
    static TextStyle _styles[TextStylesCount];
};


#endif //UGAME_THEME_H
