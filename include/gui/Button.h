//
// Created by symbx on 22.03.17.
//

#ifndef UGAME_BUTTON_H
#define UGAME_BUTTON_H


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Text.hpp>

class Button : public sf::Drawable {
public:
    enum State {
        Normal,
        Hover,
        Press,
        Focus,
        Disabled
    };
    Button(int x, int y, int w, int h, const std::wstring& text);
    void setPosition(int x, int y);
    void setString(std::wstring& text);
    bool event(sf::Event& event);
    void focus(bool f);
    void disable(bool d);
    bool isAction();
    sf::Int8 isMoveFocus();
    bool isFocus();

private:
    sf::VertexArray _vertex;
    int _posX;
    int _posY;
    int _width;
    int _height;
    sf::Transform _transform;
    sf::Text _text;
    State _state;
    bool _action;
    sf::Int8 _focus;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void _centerText();
    void _setState(State state);
    bool _containsPointer(float x, float y);
};


#endif //UGAME_BUTTON_H
