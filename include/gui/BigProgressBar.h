//
// Created by symbx on 22.03.17.
//

#ifndef UGAME_BIGPROGRESSBAR_H
#define UGAME_BIGPROGRESSBAR_H


#include <SFML/Config.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>

class BigProgressBar : public sf::Drawable {
public:
    BigProgressBar(int x, int y, float val);
    void setPosition(int x, int y);
    void setValue(float val);
    void disable(bool d);

private:
    sf::VertexArray _vertex;
    sf::Transform _transform;
    bool _disabled;
    float _value;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};


#endif //UGAME_BIGPROGRESSBAR_H
