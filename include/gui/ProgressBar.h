//
// Created by symbx on 22.03.17.
//

#ifndef UGAME_PROGRESSBAR_H
#define UGAME_PROGRESSBAR_H

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>

class ProgressBar  : public sf::Drawable {
public:
    ProgressBar(int x, int y, float val);
    void setPosition(int x, int y);
    void setValue(float val);
    void disable(bool d);

private:
    sf::VertexArray _vertex;
    sf::Transform _transform;
    bool _disabled;
    float _value;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};


#endif //UGAME_PROGRESSBAR_H
