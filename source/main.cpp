#include <iostream>
#include "../include/utils/Utils.h"
#include "../include/utils/Logger.h"
#include "../include/core/Application.h"

int main(int argc, char* argv[]) {
    char* appPath = Utils::getAppDir();
    std::string appDir(appPath);
    delete[] appPath;
    Logger::init(appDir);

    Application* app = new Application(appDir);
    //app->args(argc, argv);
    app->run();

    delete app;
    Logger::clean();
}