//
// Created by symbx on 22.03.17.
//

#include "../../include/gui/ProgressBar.h"
#include "../../include/gui/Theme.h"

ProgressBar::ProgressBar(int x, int y, float val) :
        _vertex(sf::Quads, 8) {
    if(val > 1)
        val = 1;
    if(val < 0)
        val = 0;
    _value = val;

    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);

    sf::IntRect textureBg = Theme::getTextureRect(Theme::TextureProgressBgE);
    sf::IntRect textureBar = Theme::getTextureRect(Theme::TextureProgressBarE);
    sf::IntRect offset = Theme::getTextureRect(Theme::TextureProgressOffset);

    //bg (pos)
    _vertex[0].position = sf::Vector2f(0, 0);
    _vertex[1].position = sf::Vector2f(textureBg.width, 0);
    _vertex[2].position = sf::Vector2f(textureBg.width, textureBg.height);
    _vertex[3].position = sf::Vector2f(0, textureBg.height);

    //bg (texture)
    _vertex[0].texCoords = sf::Vector2f(textureBg.left, textureBg.top);
    _vertex[1].texCoords = sf::Vector2f(textureBg.left + textureBg.width, textureBg.top);
    _vertex[2].texCoords = sf::Vector2f(textureBg.left + textureBg.width, textureBg.top + textureBg.height);
    _vertex[3].texCoords = sf::Vector2f(textureBg.left, textureBg.top + textureBg.height);

    float size = val * offset.width;
    //progress (pos, val width)
    _vertex[4].position = sf::Vector2f(offset.left, offset.top);
    _vertex[5].position = sf::Vector2f(offset.left + size, offset.top);
    _vertex[6].position = sf::Vector2f(offset.left + size, offset.top + offset.height);
    _vertex[7].position = sf::Vector2f(offset.left, offset.top + offset.height);

    //progress (texture, val width)
    _vertex[4].texCoords = sf::Vector2f(textureBar.left, textureBar.top);
    _vertex[5].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top);
    _vertex[6].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top + textureBar.height);
    _vertex[7].texCoords = sf::Vector2f(textureBar.left, textureBar.top + textureBar.height);

    _disabled = false;
}

void ProgressBar::setPosition(int x, int y) {
    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);
}

void ProgressBar::setValue(float val) {
    if(val > 1)
        val = 1;
    if(val < 0)
        val = 0;
    if(_value == val || _disabled)
        return;
    _value = val;
    sf::IntRect textureBar = Theme::getTextureRect(Theme::TextureProgressBarE);
    sf::IntRect offset = Theme::getTextureRect(Theme::TextureProgressOffset);
    float size = _value * offset.width;
    //progress (pos, val width)
    _vertex[5].position = sf::Vector2f(offset.left + size, offset.top);
    _vertex[6].position = sf::Vector2f(offset.left + size, offset.top + offset.height);

    //progress (texture, val width)
    _vertex[5].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top);
    _vertex[6].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top + textureBar.height);
}

void ProgressBar::disable(bool d) {
    if(_disabled == d)
        return;
    _disabled = d;

    sf::IntRect textureBg = Theme::getTextureRect(_disabled ? Theme::TextureProgressBgD : Theme::TextureProgressBgE);
    sf::IntRect textureBar = Theme::getTextureRect(_disabled ? Theme::TextureProgressBarD : Theme::TextureProgressBarE);

    //bg (texture)
    _vertex[0].texCoords = sf::Vector2f(textureBg.left, textureBg.top);
    _vertex[1].texCoords = sf::Vector2f(textureBg.left + textureBg.width, textureBg.top);
    _vertex[2].texCoords = sf::Vector2f(textureBg.left + textureBg.width, textureBg.top + textureBg.height);
    _vertex[3].texCoords = sf::Vector2f(textureBg.left, textureBg.top + textureBg.height);

    float size = _value * textureBar.width;
    //progress (texture, val width)
    _vertex[4].texCoords = sf::Vector2f(textureBar.left, textureBar.top);
    _vertex[5].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top);
    _vertex[6].texCoords = sf::Vector2f(textureBar.left + size, textureBar.top + textureBar.height);
    _vertex[7].texCoords = sf::Vector2f(textureBar.left, textureBar.top + textureBar.height);
}

void ProgressBar::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= _transform;
    states.texture = &Theme::getSpriteSheet();
    target.draw(_vertex, states);
}
