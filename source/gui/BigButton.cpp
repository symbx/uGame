//
// Created by symbx on 22.03.17.
//

#include "../../include/gui/BigButton.h"
#include "../../include/gui/Theme.h"

BigButton::BigButton(int x, int y, int size, const std::wstring &text) :
    _vertex(sf::TrianglesStrip, 8),
    _text() {
    sf::IntRect textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnNLeft);
    sf::IntRect textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnNMid);
    sf::IntRect textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnNRight);

    _posX = x;
    _posY = y;
    _width = textureRectLeft.width + size + textureRectRight.width;
    _height = textureRectMid.height; //I think all 3 part same height;
    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);
    //tmp for shifting
    int shift = 0;

    //Left part (shift)
    _vertex[0].position = sf::Vector2f(shift, 0);
    _vertex[1].position = sf::Vector2f(shift, textureRectLeft.height);
    _vertex[2].position = sf::Vector2f(textureRectLeft.width, 0);
    _vertex[3].position = sf::Vector2f(textureRectLeft.width, textureRectLeft.height);
    shift += textureRectLeft.width;
    //Left part (texture)
    _vertex[0].texCoords = sf::Vector2f(textureRectLeft.left, textureRectLeft.top);
    _vertex[1].texCoords = sf::Vector2f(textureRectLeft.left, textureRectLeft.top + textureRectLeft.height);
    _vertex[2].texCoords = sf::Vector2f(textureRectLeft.left + textureRectLeft.width, textureRectLeft.top);
    _vertex[3].texCoords = sf::Vector2f(textureRectLeft.left + textureRectLeft.width, textureRectLeft.top + textureRectLeft.height);

    //Mid part (shift)
    _vertex[4].position = sf::Vector2f(shift + size, 0);
    _vertex[5].position = sf::Vector2f(shift + size, textureRectMid.height);
    shift += size;
    //Mid part (texture)
    _vertex[4].texCoords = sf::Vector2f(textureRectMid.left + textureRectMid.width, textureRectMid.top);
    _vertex[5].texCoords = sf::Vector2f(textureRectMid.left + textureRectMid.width, textureRectMid.top + textureRectMid.height);

    //Right part (shift)
    _vertex[6].position = sf::Vector2f(shift + textureRectRight.width, 0);
    _vertex[7].position = sf::Vector2f(shift + textureRectRight.width, textureRectRight.height);
    //Right part (texture)
    _vertex[6].texCoords = sf::Vector2f(textureRectRight.left + textureRectRight.width, textureRectRight.top);
    _vertex[7].texCoords = sf::Vector2f(textureRectRight.left + textureRectRight.width, textureRectRight.top + textureRectRight.height);

    Theme::TextStyle style = Theme::getTextStyle(Theme::TextStyleBigBtnN);
    _text.setString(text);
    _text.setFont(Theme::getFont());
    _text.setCharacterSize(style.size);
    _text.setStyle(style.style);
    _text.setFillColor(style.color);
    _text.setPosition(sf::Vector2f(0, 0));
    _centerText();
    _state = Normal;
    _action = false;
    _focus = 0;
}

void BigButton::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= _transform;
    states.texture = &Theme::getSpriteSheet();
    target.draw(_vertex, states);
    target.draw(_text, states);
}

void BigButton::setPosition(int x, int y) {
    _posX = x;
    _posY = y;
    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);
}

void BigButton::_centerText() {
    sf::FloatRect size = _text.getLocalBounds();
    sf::IntRect offset = Theme::getTextureRect(Theme::TextureBigBtnOffset);
    _text.setPosition((_width-size.width)/2.0f, (_height-size.height)/2.0f - offset.height);
}

void BigButton::setString(std::wstring& text) {
    _text.setString(text);
    _centerText();
}

bool BigButton::event(sf::Event& event) {
    if(event.type == sf::Event::MouseMoved) {
        if(_containsPointer(event.mouseMove.x, event.mouseMove.y)) {
            if(_state == Normal) {
                _setState(Hover);
                return true;
            }
        } else {
            if(_state == Hover) {
                _setState(Normal);
                return true;
            }
        }
    }
    if(event.type == sf::Event::MouseButtonPressed) {
        if(_containsPointer(event.mouseButton.x, event.mouseButton.y)) {
            //If mouse press - it hover or focus, but...may be normal?
            if(_state == Hover || _state == Focus || _state == Normal) {
                _setState(Press);
                return true;
            }
        }
        if(_state == Press) {
            _setState(Hover);
        }
    }
    if(event.type == sf::Event::MouseButtonReleased) {
        if(_state == Press) {
            _setState(Focus);
            if(_containsPointer(event.mouseButton.x, event.mouseButton.y)) {
                _action = true;
                return true;
            }
        }
    }
    if(event.type == sf::Event::KeyPressed && _state == Focus) {
        if(event.key.code == sf::Keyboard::Return) {
            _action = true;
            return true;
        }
        if(event.key.code == sf::Keyboard::Tab) {
            _focus = (sf::Int8) (event.key.shift ? -1 : 1);
            return true;
        }
        if(event.key.code == sf::Keyboard::Up) {
            _focus = -1;
            return true;
        }
        if(event.key.code == sf::Keyboard::Down) {
            _focus = 1;
            return true;
        }
    }
    return false;
}

void BigButton::focus(bool f) {
    _setState(f ? Focus : Normal);
}

void BigButton::_setState(BigButton::State state) {
    if(state == _state)
        return;
    _state = state;
    sf::IntRect textureRectLeft;
    sf::IntRect textureRectMid;
    sf::IntRect textureRectRight;
    switch (state) {
        case Normal:
            textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnNLeft);
            textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnNMid);
            textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnNRight);
            break;
        case Hover:
            textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnHLeft);
            textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnHMid);
            textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnHRight);
            break;
        case Press:
            textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnPLeft);
            textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnPMid);
            textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnPRight);
            break;
        case Focus:
            textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnFLeft);
            textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnFMid);
            textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnFRight);
            break;
        case Disabled:
            textureRectLeft = Theme::getTextureRect(Theme::TextureBigBtnDLeft);
            textureRectMid = Theme::getTextureRect(Theme::TextureBigBtnDMid);
            textureRectRight = Theme::getTextureRect(Theme::TextureBigBtnDRight);
            break;
    }
    //Left part (texture)
    _vertex[0].texCoords = sf::Vector2f(textureRectLeft.left, textureRectLeft.top);
    _vertex[1].texCoords = sf::Vector2f(textureRectLeft.left, textureRectLeft.top + textureRectLeft.height);
    _vertex[2].texCoords = sf::Vector2f(textureRectLeft.left + textureRectLeft.width, textureRectLeft.top);
    _vertex[3].texCoords = sf::Vector2f(textureRectLeft.left + textureRectLeft.width, textureRectLeft.top + textureRectLeft.height);
    //Mid part (texture)
    _vertex[4].texCoords = sf::Vector2f(textureRectMid.left + textureRectMid.width, textureRectMid.top);
    _vertex[5].texCoords = sf::Vector2f(textureRectMid.left + textureRectMid.width, textureRectMid.top + textureRectMid.height);
    //Right part (texture)
    _vertex[6].texCoords = sf::Vector2f(textureRectRight.left + textureRectRight.width, textureRectRight.top);
    _vertex[7].texCoords = sf::Vector2f(textureRectRight.left + textureRectRight.width, textureRectRight.top + textureRectRight.height);
}

void BigButton::disable(bool d) {
    _setState(d ? Disabled : Normal);
}

bool BigButton::_containsPointer(float x, float y) {
    x -= _posX;
    y -= _posY;
    return (x > 0 && y > 0 && x < _width && y < _height);
}

bool BigButton::isAction() {
    if(_action) {
        _action = false;
        return true;
    }
    return false;
}

sf::Int8 BigButton::isMoveFocus() {
    if(_focus != 0) {
        sf::Int8 tmp = _focus;
        _focus = 0;
        return tmp;
    }
    return 0;
}

bool BigButton::isFocus() {
    return _state == Focus;
}
