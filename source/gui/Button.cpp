//
// Created by symbx on 22.03.17.
//

#include "../../include/gui/Button.h"
#include "../../include/gui/Theme.h"

Button::Button(int x, int y, int w, int h, const std::wstring& text) :
        _vertex(sf::Triangles, 54),
        _text() {
    sf::IntRect textureRectTL = Theme::getTextureRect(Theme::TextureBtnN_TL);
    sf::IntRect textureRectT = Theme::getTextureRect(Theme::TextureBtnN_T);
    sf::IntRect textureRectTR = Theme::getTextureRect(Theme::TextureBtnN_TR);
    sf::IntRect textureRectR = Theme::getTextureRect(Theme::TextureBtnN_R);
    sf::IntRect textureRectBR = Theme::getTextureRect(Theme::TextureBtnN_BR);
    sf::IntRect textureRectB = Theme::getTextureRect(Theme::TextureBtnN_B);
    sf::IntRect textureRectBL = Theme::getTextureRect(Theme::TextureBtnN_BL);
    sf::IntRect textureRectL = Theme::getTextureRect(Theme::TextureBtnN_L);
    sf::IntRect textureRectBG = Theme::getTextureRect(Theme::TextureBtnN_BG);

    _posX = x;
    _posY = y;
    _width = textureRectL.width + w + textureRectR.width;
    _height = textureRectT.height + h + textureRectB.height;
    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);
    //tmp for shifting
    int shiftX = 0;
    int shiftY = 0;

    //TopLeft (pos)
    _vertex[0].position = sf::Vector2f(0, 0);
    _vertex[1].position = sf::Vector2f(0, textureRectTL.height);
    _vertex[2].position = sf::Vector2f(textureRectTL.width, 0);
    _vertex[3].position = _vertex[1].position;
    _vertex[4].position = _vertex[2].position;
    _vertex[5].position = sf::Vector2f(textureRectTL.width, textureRectTL.height);
    shiftX += textureRectTL.width;

    //TopLeft (texture)
    _vertex[0].texCoords = sf::Vector2f(textureRectTL.left, textureRectTL.top);
    _vertex[1].texCoords = sf::Vector2f(textureRectTL.left, textureRectTL.top + textureRectTL.height);
    _vertex[2].texCoords = sf::Vector2f(textureRectTL.left + textureRectTL.width, textureRectTL.top);
    _vertex[3].texCoords = _vertex[1].texCoords;
    _vertex[4].texCoords = _vertex[2].texCoords;
    _vertex[5].texCoords = sf::Vector2f(textureRectTL.left + textureRectTL.width, textureRectTL.top + textureRectTL.height);

    //Top (pos)
    _vertex[6].position = sf::Vector2f(shiftX, 0);
    _vertex[7].position = sf::Vector2f(shiftX, textureRectT.height);
    _vertex[8].position = sf::Vector2f(shiftX + w, 0);
    _vertex[9].position = _vertex[7].position;
    _vertex[10].position = _vertex[8].position;
    _vertex[11].position = sf::Vector2f(shiftX + w, textureRectT.height);
    shiftX += w;

    //Top (texture)
    _vertex[6].texCoords = sf::Vector2f(textureRectT.left, textureRectT.top);
    _vertex[7].texCoords = sf::Vector2f(textureRectT.left, textureRectT.top + textureRectT.height);
    _vertex[8].texCoords = sf::Vector2f(textureRectT.left + textureRectT.width, textureRectT.top);
    _vertex[9].texCoords = _vertex[7].texCoords;
    _vertex[10].texCoords = _vertex[8].texCoords;
    _vertex[11].texCoords = sf::Vector2f(textureRectT.left + textureRectT.width, textureRectT.top + textureRectT.height);

    //TopRight (pos)
    _vertex[12].position = sf::Vector2f(shiftX, 0);
    _vertex[13].position = sf::Vector2f(shiftX, textureRectTR.height);
    _vertex[14].position = sf::Vector2f(shiftX + textureRectTR.width, 0);
    _vertex[15].position = _vertex[13].position;
    _vertex[16].position = _vertex[14].position;
    _vertex[17].position = sf::Vector2f(shiftX + textureRectTR.width, textureRectTR.height);
    shiftX = 0;
    shiftY += textureRectT.height;

    //TopRight (texture)
    _vertex[12].texCoords = sf::Vector2f(textureRectTR.left, textureRectTR.top);
    _vertex[13].texCoords = sf::Vector2f(textureRectTR.left, textureRectTR.top + textureRectTR.height);
    _vertex[14].texCoords = sf::Vector2f(textureRectTR.left + textureRectTR.width, textureRectTR.top);
    _vertex[15].texCoords = _vertex[13].texCoords;
    _vertex[16].texCoords = _vertex[14].texCoords;
    _vertex[17].texCoords = sf::Vector2f(textureRectTR.left + textureRectTR.width, textureRectTR.top + textureRectTR.height);

    //Left (pos)
    _vertex[18].position = sf::Vector2f(0, shiftY);
    _vertex[19].position = sf::Vector2f(0, h + shiftY);
    _vertex[20].position = sf::Vector2f(textureRectL.width, shiftY);
    _vertex[21].position = _vertex[19].position;
    _vertex[22].position = _vertex[20].position;
    _vertex[23].position = sf::Vector2f(textureRectL.width, h + shiftY);
    shiftX += textureRectL.width;

    //Left (texture)
    _vertex[18].texCoords = sf::Vector2f(textureRectL.left, textureRectL.top);
    _vertex[19].texCoords = sf::Vector2f(textureRectL.left, textureRectL.top + textureRectL.height);
    _vertex[20].texCoords = sf::Vector2f(textureRectL.left + textureRectL.width, textureRectL.top);
    _vertex[21].texCoords = _vertex[19].texCoords;
    _vertex[22].texCoords = _vertex[20].texCoords;
    _vertex[23].texCoords = sf::Vector2f(textureRectL.left + textureRectL.width, textureRectL.top + textureRectL.height);

    //BG (pos)
    _vertex[24].position = sf::Vector2f(shiftX, shiftY);
    _vertex[25].position = sf::Vector2f(shiftX, shiftY + h);
    _vertex[26].position = sf::Vector2f(shiftX + w, shiftY);
    _vertex[27].position = _vertex[25].position;
    _vertex[28].position = _vertex[26].position;
    _vertex[29].position = sf::Vector2f(shiftX + w, shiftY + h);
    shiftX += w;

    //BG (texture)
    _vertex[24].texCoords = sf::Vector2f(textureRectBG.left, textureRectBG.top);
    _vertex[25].texCoords = sf::Vector2f(textureRectBG.left, textureRectBG.top + textureRectBG.height);
    _vertex[26].texCoords = sf::Vector2f(textureRectBG.left + textureRectBG.width, textureRectBG.top);
    _vertex[27].texCoords = _vertex[25].texCoords;
    _vertex[28].texCoords = _vertex[26].texCoords;
    _vertex[29].texCoords = sf::Vector2f(textureRectBG.left + textureRectBG.width, textureRectBG.top + textureRectBG.height);

    //Right (pos)
    _vertex[30].position = sf::Vector2f(shiftX, shiftY);
    _vertex[31].position = sf::Vector2f(shiftX, h + shiftY);
    _vertex[32].position = sf::Vector2f(shiftX + textureRectR.width, shiftY);
    _vertex[33].position = _vertex[31].position;
    _vertex[34].position = _vertex[32].position;
    _vertex[35].position = sf::Vector2f(shiftX + textureRectR.width, h + shiftY);
    shiftX = 0;
    shiftY += h;

    //Right (texture)
    _vertex[30].texCoords = sf::Vector2f(textureRectR.left, textureRectR.top);
    _vertex[31].texCoords = sf::Vector2f(textureRectR.left, textureRectR.top + textureRectR.height);
    _vertex[32].texCoords = sf::Vector2f(textureRectR.left + textureRectR.width, textureRectR.top);
    _vertex[33].texCoords = _vertex[31].texCoords;
    _vertex[34].texCoords = _vertex[32].texCoords;
    _vertex[35].texCoords = sf::Vector2f(textureRectR.left + textureRectR.width, textureRectR.top + textureRectR.height);

    //BottomLeft (pos)
    _vertex[36].position = sf::Vector2f(0, shiftY);
    _vertex[37].position = sf::Vector2f(0, shiftY + textureRectBL.height);
    _vertex[38].position = sf::Vector2f(textureRectBL.width, shiftY);
    _vertex[39].position = _vertex[37].position;
    _vertex[40].position = _vertex[38].position;
    _vertex[41].position = sf::Vector2f(textureRectBL.width, shiftY + textureRectBL.height);
    shiftX += textureRectBL.width;

    //BottomLeft (texture)
    _vertex[36].texCoords = sf::Vector2f(textureRectBL.left, textureRectBL.top);
    _vertex[37].texCoords = sf::Vector2f(textureRectBL.left, textureRectBL.top + textureRectBL.height);
    _vertex[38].texCoords = sf::Vector2f(textureRectBL.left + textureRectBL.width, textureRectBL.top);
    _vertex[39].texCoords = _vertex[37].texCoords;
    _vertex[40].texCoords = _vertex[38].texCoords;
    _vertex[41].texCoords = sf::Vector2f(textureRectBL.left + textureRectBL.width, textureRectBL.top + textureRectBL.height);

    //Bottom (pos)
    _vertex[42].position = sf::Vector2f(shiftX, shiftY);
    _vertex[43].position = sf::Vector2f(shiftX, shiftY + textureRectB.height);
    _vertex[44].position = sf::Vector2f(shiftX + w, shiftY);
    _vertex[45].position = _vertex[43].position;
    _vertex[46].position = _vertex[44].position;
    _vertex[47].position = sf::Vector2f(shiftX + w, shiftY + textureRectB.height);
    shiftX += w;

    //Bottom (texture)
    _vertex[42].texCoords = sf::Vector2f(textureRectB.left, textureRectB.top);
    _vertex[43].texCoords = sf::Vector2f(textureRectB.left, textureRectB.top + textureRectB.height);
    _vertex[44].texCoords = sf::Vector2f(textureRectB.left + textureRectB.width, textureRectB.top);
    _vertex[45].texCoords = _vertex[43].texCoords;
    _vertex[46].texCoords = _vertex[44].texCoords;
    _vertex[47].texCoords = sf::Vector2f(textureRectB.left + textureRectB.width, textureRectB.top + textureRectB.height);

    //BottomRight (pos)
    _vertex[48].position = sf::Vector2f(shiftX, shiftY);
    _vertex[49].position = sf::Vector2f(shiftX, shiftY + textureRectBR.height);
    _vertex[50].position = sf::Vector2f(shiftX + textureRectBR.width, shiftY);
    _vertex[51].position = _vertex[49].position;
    _vertex[52].position = _vertex[50].position;
    _vertex[53].position = sf::Vector2f(shiftX + textureRectBR.width, shiftY + textureRectBR.height);

    //BottomRight (texture)
    _vertex[48].texCoords = sf::Vector2f(textureRectBR.left, textureRectBR.top);
    _vertex[49].texCoords = sf::Vector2f(textureRectBR.left, textureRectBR.top + textureRectBR.height);
    _vertex[50].texCoords = sf::Vector2f(textureRectBR.left + textureRectBR.width, textureRectBR.top);
    _vertex[51].texCoords = _vertex[49].texCoords;
    _vertex[52].texCoords = _vertex[50].texCoords;
    _vertex[53].texCoords = sf::Vector2f(textureRectBR.left + textureRectBR.width, textureRectBR.top + textureRectBR.height);

    Theme::TextStyle style = Theme::getTextStyle(Theme::TextStyleBigBtnN);
    _text.setString(text);
    _text.setFont(Theme::getFont());
    _text.setCharacterSize(style.size);
    _text.setStyle(style.style);
    _text.setFillColor(style.color);
    _text.setPosition(sf::Vector2f(0, 0));
    _centerText();
    _state = Normal;
    _action = false;
    _focus = 0;
}

void Button::setPosition(int x, int y) {
    _posX = x;
    _posY = y;
    _transform = sf::Transform(1, 0, x,
                               0, 1, y,
                               0, 0, 1);
}

void Button::setString(std::wstring& text) {
    _text.setString(text);
    _centerText();
}


bool Button::event(sf::Event& event) {
    if(event.type == sf::Event::MouseMoved) {
        if(_containsPointer(event.mouseMove.x, event.mouseMove.y)) {
            if(_state == Normal) {
                _setState(Hover);
                return true;
            }
        } else {
            if(_state == Hover) {
                _setState(Normal);
                return true;
            }
        }
    }
    if(event.type == sf::Event::MouseButtonPressed) {
        if(_containsPointer(event.mouseButton.x, event.mouseButton.y)) {
            //If mouse press - it hover or focus, but...may be normal?
            if(_state == Hover || _state == Focus || _state == Normal) {
                _setState(Press);
                return true;
            }
        }
        if(_state == Press) {
            _setState(Hover);
        }
    }
    if(event.type == sf::Event::MouseButtonReleased) {
        if(_state == Press) {
            _setState(Focus);
            if(_containsPointer(event.mouseButton.x, event.mouseButton.y)) {
                _action = true;
                return true;
            }
        }
    }
    if(event.type == sf::Event::KeyPressed && _state == Focus) {
        if(event.key.code == sf::Keyboard::Return) {
            _action = true;
            return true;
        }
        if(event.key.code == sf::Keyboard::Tab) {
            _focus = (sf::Int8) (event.key.shift ? -1 : 1);
            return true;
        }
        if(event.key.code == sf::Keyboard::Up) {
            _focus = -1;
            return true;
        }
        if(event.key.code == sf::Keyboard::Down) {
            _focus = 1;
            return true;
        }
    }
    return false;
}

void Button::focus(bool f) {
    _setState(f ? Focus : Normal);
}

void Button::disable(bool d) {
    _setState(d ? Disabled : Normal);
}

bool Button::isAction() {
    if(_action) {
        _action = false;
        return true;
    }
    return false;
}

sf::Int8 Button::isMoveFocus() {
    if(_focus != 0) {
        sf::Int8 tmp = _focus;
        _focus = 0;
        return tmp;
    }
    return 0;
}

bool Button::isFocus() {
    return _state == Focus;
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= _transform;
    states.texture = &Theme::getSpriteSheet();
    target.draw(_vertex, states);
    target.draw(_text, states);
}

void Button::_centerText() {
    sf::FloatRect size = _text.getLocalBounds();
    sf::IntRect offset = Theme::getTextureRect(Theme::TextureBtnOffset);
    _text.setPosition((_width-size.width)/2.0f, (_height-size.height)/2.0f - offset.height);
}

void Button::_setState(Button::State state) {
    if(state == _state)
        return;
    _state = state;
    sf::IntRect textureRectTL;
    sf::IntRect textureRectT;
    sf::IntRect textureRectTR;
    sf::IntRect textureRectR;
    sf::IntRect textureRectBR;
    sf::IntRect textureRectB;
    sf::IntRect textureRectBL;
    sf::IntRect textureRectL;
    sf::IntRect textureRectBG;
    switch (state) {
        case Normal:
            textureRectTL = Theme::getTextureRect(Theme::TextureBtnN_TL);
            textureRectT = Theme::getTextureRect(Theme::TextureBtnN_T);
            textureRectTR = Theme::getTextureRect(Theme::TextureBtnN_TR);
            textureRectR = Theme::getTextureRect(Theme::TextureBtnN_R);
            textureRectBR = Theme::getTextureRect(Theme::TextureBtnN_BR);
            textureRectB = Theme::getTextureRect(Theme::TextureBtnN_B);
            textureRectBL = Theme::getTextureRect(Theme::TextureBtnN_BL);
            textureRectL = Theme::getTextureRect(Theme::TextureBtnN_L);
            textureRectBG = Theme::getTextureRect(Theme::TextureBtnN_BG);
            break;
        case Hover:
            textureRectTL = Theme::getTextureRect(Theme::TextureBtnH_TL);
            textureRectT = Theme::getTextureRect(Theme::TextureBtnH_T);
            textureRectTR = Theme::getTextureRect(Theme::TextureBtnH_TR);
            textureRectR = Theme::getTextureRect(Theme::TextureBtnH_R);
            textureRectBR = Theme::getTextureRect(Theme::TextureBtnH_BR);
            textureRectB = Theme::getTextureRect(Theme::TextureBtnH_B);
            textureRectBL = Theme::getTextureRect(Theme::TextureBtnH_BL);
            textureRectL = Theme::getTextureRect(Theme::TextureBtnH_L);
            textureRectBG = Theme::getTextureRect(Theme::TextureBtnH_BG);
            break;
        case Press:
            textureRectTL = Theme::getTextureRect(Theme::TextureBtnP_TL);
            textureRectT = Theme::getTextureRect(Theme::TextureBtnP_T);
            textureRectTR = Theme::getTextureRect(Theme::TextureBtnP_TR);
            textureRectR = Theme::getTextureRect(Theme::TextureBtnP_R);
            textureRectBR = Theme::getTextureRect(Theme::TextureBtnP_BR);
            textureRectB = Theme::getTextureRect(Theme::TextureBtnP_B);
            textureRectBL = Theme::getTextureRect(Theme::TextureBtnP_BL);
            textureRectL = Theme::getTextureRect(Theme::TextureBtnP_L);
            textureRectBG = Theme::getTextureRect(Theme::TextureBtnP_BG);
            break;
        case Focus:
            textureRectTL = Theme::getTextureRect(Theme::TextureBtnF_TL);
            textureRectT = Theme::getTextureRect(Theme::TextureBtnF_T);
            textureRectTR = Theme::getTextureRect(Theme::TextureBtnF_TR);
            textureRectR = Theme::getTextureRect(Theme::TextureBtnF_R);
            textureRectBR = Theme::getTextureRect(Theme::TextureBtnF_BR);
            textureRectB = Theme::getTextureRect(Theme::TextureBtnF_B);
            textureRectBL = Theme::getTextureRect(Theme::TextureBtnF_BL);
            textureRectL = Theme::getTextureRect(Theme::TextureBtnF_L);
            textureRectBG = Theme::getTextureRect(Theme::TextureBtnF_BG);
            break;
        case Disabled:
            textureRectTL = Theme::getTextureRect(Theme::TextureBtnD_TL);
            textureRectT = Theme::getTextureRect(Theme::TextureBtnD_T);
            textureRectTR = Theme::getTextureRect(Theme::TextureBtnD_TR);
            textureRectR = Theme::getTextureRect(Theme::TextureBtnD_R);
            textureRectBR = Theme::getTextureRect(Theme::TextureBtnD_BR);
            textureRectB = Theme::getTextureRect(Theme::TextureBtnD_B);
            textureRectBL = Theme::getTextureRect(Theme::TextureBtnD_BL);
            textureRectL = Theme::getTextureRect(Theme::TextureBtnD_L);
            textureRectBG = Theme::getTextureRect(Theme::TextureBtnD_BG);
            break;
    }
    //TopLeft (texture)
    _vertex[0].texCoords = sf::Vector2f(textureRectTL.left, textureRectTL.top);
    _vertex[1].texCoords = sf::Vector2f(textureRectTL.left, textureRectTL.top + textureRectTL.height);
    _vertex[2].texCoords = sf::Vector2f(textureRectTL.left + textureRectTL.width, textureRectTL.top);
    _vertex[3].texCoords = _vertex[1].texCoords;
    _vertex[4].texCoords = _vertex[2].texCoords;
    _vertex[5].texCoords = sf::Vector2f(textureRectTL.left + textureRectTL.width, textureRectTL.top + textureRectTL.height);

    //Top (texture)
    _vertex[6].texCoords = sf::Vector2f(textureRectT.left, textureRectT.top);
    _vertex[7].texCoords = sf::Vector2f(textureRectT.left, textureRectT.top + textureRectT.height);
    _vertex[8].texCoords = sf::Vector2f(textureRectT.left + textureRectT.width, textureRectT.top);
    _vertex[9].texCoords = _vertex[7].texCoords;
    _vertex[10].texCoords = _vertex[8].texCoords;
    _vertex[11].texCoords = sf::Vector2f(textureRectT.left + textureRectT.width, textureRectT.top + textureRectT.height);

    //TopRight (texture)
    _vertex[12].texCoords = sf::Vector2f(textureRectTR.left, textureRectTR.top);
    _vertex[13].texCoords = sf::Vector2f(textureRectTR.left, textureRectTR.top + textureRectTR.height);
    _vertex[14].texCoords = sf::Vector2f(textureRectTR.left + textureRectTR.width, textureRectTR.top);
    _vertex[15].texCoords = _vertex[13].texCoords;
    _vertex[16].texCoords = _vertex[14].texCoords;
    _vertex[17].texCoords = sf::Vector2f(textureRectTR.left + textureRectTR.width, textureRectTR.top + textureRectTR.height);

    //Left (texture)
    _vertex[18].texCoords = sf::Vector2f(textureRectL.left, textureRectL.top);
    _vertex[19].texCoords = sf::Vector2f(textureRectL.left, textureRectL.top + textureRectL.height);
    _vertex[20].texCoords = sf::Vector2f(textureRectL.left + textureRectL.width, textureRectL.top);
    _vertex[21].texCoords = _vertex[19].texCoords;
    _vertex[22].texCoords = _vertex[20].texCoords;
    _vertex[23].texCoords = sf::Vector2f(textureRectL.left + textureRectL.width, textureRectL.top + textureRectL.height);

    //BG (texture)
    _vertex[24].texCoords = sf::Vector2f(textureRectBG.left, textureRectBG.top);
    _vertex[25].texCoords = sf::Vector2f(textureRectBG.left, textureRectBG.top + textureRectBG.height);
    _vertex[26].texCoords = sf::Vector2f(textureRectBG.left + textureRectBG.width, textureRectBG.top);
    _vertex[27].texCoords = _vertex[25].texCoords;
    _vertex[28].texCoords = _vertex[26].texCoords;
    _vertex[29].texCoords = sf::Vector2f(textureRectBG.left + textureRectBG.width, textureRectBG.top + textureRectBG.height);

    //Right (texture)
    _vertex[30].texCoords = sf::Vector2f(textureRectR.left, textureRectR.top);
    _vertex[31].texCoords = sf::Vector2f(textureRectR.left, textureRectR.top + textureRectR.height);
    _vertex[32].texCoords = sf::Vector2f(textureRectR.left + textureRectR.width, textureRectR.top);
    _vertex[33].texCoords = _vertex[31].texCoords;
    _vertex[34].texCoords = _vertex[32].texCoords;
    _vertex[35].texCoords = sf::Vector2f(textureRectR.left + textureRectR.width, textureRectR.top + textureRectR.height);

    //BottomLeft (texture)
    _vertex[36].texCoords = sf::Vector2f(textureRectBL.left, textureRectBL.top);
    _vertex[37].texCoords = sf::Vector2f(textureRectBL.left, textureRectBL.top + textureRectBL.height);
    _vertex[38].texCoords = sf::Vector2f(textureRectBL.left + textureRectBL.width, textureRectBL.top);
    _vertex[39].texCoords = _vertex[37].texCoords;
    _vertex[40].texCoords = _vertex[38].texCoords;
    _vertex[41].texCoords = sf::Vector2f(textureRectBL.left + textureRectBL.width, textureRectBL.top + textureRectBL.height);

    //Bottom (texture)
    _vertex[42].texCoords = sf::Vector2f(textureRectB.left, textureRectB.top);
    _vertex[43].texCoords = sf::Vector2f(textureRectB.left, textureRectB.top + textureRectB.height);
    _vertex[44].texCoords = sf::Vector2f(textureRectB.left + textureRectB.width, textureRectB.top);
    _vertex[45].texCoords = _vertex[43].texCoords;
    _vertex[46].texCoords = _vertex[44].texCoords;
    _vertex[47].texCoords = sf::Vector2f(textureRectB.left + textureRectB.width, textureRectB.top + textureRectB.height);

    //BottomRight (texture)
    _vertex[48].texCoords = sf::Vector2f(textureRectBR.left, textureRectBR.top);
    _vertex[49].texCoords = sf::Vector2f(textureRectBR.left, textureRectBR.top + textureRectBR.height);
    _vertex[50].texCoords = sf::Vector2f(textureRectBR.left + textureRectBR.width, textureRectBR.top);
    _vertex[51].texCoords = _vertex[49].texCoords;
    _vertex[52].texCoords = _vertex[50].texCoords;
    _vertex[53].texCoords = sf::Vector2f(textureRectBR.left + textureRectBR.width, textureRectBR.top + textureRectBR.height);
}

bool Button::_containsPointer(float x, float y) {
    x -= _posX;
    y -= _posY;
    return (x > 0 && y > 0 && x < _width && y < _height);
}
