//
// Created by symbx on 21.03.17.
//

#include "../../include/gui/Theme.h"
#include "../../include/utils/Logger.h"

sf::Texture Theme::_spriteSheet;
sf::Font Theme::_font;
sf::InputStream* Theme::_fontStream;
sf::IntRect Theme::_rectangles[TexturesCount];
Theme::TextStyle Theme::_styles[TextStylesCount];

bool Theme::loadSpriteSheet(sf::InputStream &img, sf::InputStream &sheet) {

    if(!_spriteSheet.loadFromStream(img))
        return false;
    LOG("Loading styles");
    _rectangles[TextureEmpty] = sf::IntRect(0, 0, 0, 0);
    for(int i = TextureEmpty + 1; i < TexturesCount; i++)
    {
        sf::IntRect sub;
        sheet.read(&sub.left, 4);
        sheet.read(&sub.top, 4);
        sheet.read(&sub.width, 4);
        sheet.read(&sub.height, 4);
        LOG("Texture " + std::to_string(i) + " rect");
        LOG("From "+std::to_string(sub.left)+"x"+std::to_string(sub.top));
        LOG("Size "+std::to_string(sub.width)+"x"+std::to_string(sub.height));
        LOG("---");
        _rectangles[i] = sub;
    }
    return true;
}

const sf::Texture &Theme::getSpriteSheet() {
    return _spriteSheet;
}

bool Theme::loadFont(sf::InputStream &stream) {
    //_fontStream = &stream;
    return _font.loadFromStream(stream);
}

const sf::Font &Theme::getFont() {
    return _font;
}

void Theme::init(Application* app) {
    sf::InputStream* sprite = app->getResourceManager()->getStream("/imgs/ui/look.png");
    sf::InputStream* sheet = app->getResourceManager()->getStream("/layouts/look.style");
    loadSpriteSheet(*sprite, *sheet);
    delete sprite;
    delete sheet;
    sf::InputStream* style = app->getResourceManager()->getStream("/layouts/text.style");
    loadTextStyle(*style);
    delete style;
    _fontStream = app->getResourceManager()->getStream("/fonts/segoeui.ttf");
    loadFont(*_fontStream);
}

void Theme::clean() {
    delete _fontStream;
}

const sf::IntRect& Theme::getTextureRect(Theme::TextureType type) {
    if(type >= TexturesCount)
        return _rectangles[TextureEmpty];
    return _rectangles[type];
}

bool Theme::loadTextStyle(sf::InputStream& stream) {
    TextStyle empty;
    empty.color = sf::Color::Black;
    empty.size = 12;
    empty.style = sf::Text::Regular;
    _styles[TextStyleEmpty] = empty;

    for(int i = TextStyleEmpty + 1; i < TextStylesCount; i++)
    {
        TextStyle style;
        stream.read(&style.size, 4);
        stream.read(&style.style, 4);
        //Color in ARGB
        stream.read(&style.color.a, 1);
        stream.read(&style.color.r, 1);
        stream.read(&style.color.g, 1);
        stream.read(&style.color.b, 1);
        _styles[i] = style;
    }
    return true;
}

const Theme::TextStyle& Theme::getTextStyle(Theme::TextStyleType type) {
    if(type >= TextStylesCount)
        return _styles[TextStyleEmpty];
    return _styles[type];
}
