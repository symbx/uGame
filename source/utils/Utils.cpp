//
// Created by symbx on 21.03.17.
//

#include <string>
#include <cstring>
#ifdef _WIN32
#include <direct.h>
#else
#include <sys/stat.h>
#endif
#include "../../include/utils/Utils.h"

char* Utils::getAppDir() {
#if defined(_WIN32)
    char* home = getenv("APPDATA");
#else
    char* home = getenv("HOME");
#endif

    std::string appDir(home);
    appDir += "/.game";

#ifdef _WIN32
    _mkdir(appDir.c_str());
#else
    mkdir(appDir.c_str(), 0700);
#endif
    appDir += "/";
    char* str = new char[appDir.length()+1];
    std::strcpy(str, appDir.c_str());
    return str;
}
