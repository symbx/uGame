//
// Created by symbx on 21.03.17.
//

#include <cstdio>
#include <iostream>
#include "../../include/utils/Logger.h"

std::ofstream* Logger::_file;
time_t Logger::_rawTime;
struct tm* Logger::_timeInfo;

void Logger::init(const std::string &appDir) {
    std::string filePath = appDir + "latest.log";
    remove(filePath.c_str());
    _file = new std::ofstream(filePath.c_str());
}

void Logger::clean() {
    _file->close();
    delete _file;
}

void Logger::log(const std::string &msg, Logger::LoggerSeverity severity) {
    std::string m(_getTime());
    m += msg + "\n";
    _file->write(m.c_str(), m.length());
    _file->flush();
    std::cout << m;
}

void Logger::log(const char *msg, Logger::LoggerSeverity severity) {
    std::string m(_getTime());
    m += msg;
    m += "\n";
    _file->write(m.c_str(), m.length());
    _file->flush();
    std::cout << m;
}

void Logger::log(const std::string &msg) {
    log(msg, LoggerSeverity::SeverityDebug);
}

void Logger::log(const char *msg) {
    log(msg, LoggerSeverity::SeverityDebug);
}


std::string Logger::_getTime() {
    char buffer[12];
    time(&Logger::_rawTime);
    Logger::_timeInfo = localtime(&Logger::_rawTime);
    strftime(buffer, 12, "[%I:%M:%S] \0", Logger::_timeInfo);
    std::string str(buffer);
    return str;
}