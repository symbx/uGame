//
// Created by symbx on 21.03.17.
//

#include "../../include/state/GameStateLoading.h"
#include "../../include/Constants.h"
#include "../../include/utils/Logger.h"
#include "../../include/state/GameStateMenu.h"
#include "../../include/gui/Theme.h"

GameStateLoading::GameStateLoading() :
        _bg(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT)),
        _progress(sf::Vector2f(0, STATE_LOADING_PROGRESS_HEIGHT)) {
    _width = WINDOW_WIDTH;
    _height = WINDOW_HEIGHT;
}

void GameStateLoading::init(Application* app) {
    _app = app;
    sf::Vector2u size = app->getWindow()->getSize();
    _width = size.x;
    _height = size.y;
    _bg.setPosition(0, 0);
    _bg.setFillColor(sf::Color::White);
    _progress.setPosition(0, _height - STATE_LOADING_PROGRESS_HEIGHT);
    _progress.setFillColor(sf::Color::Black);
    _work = new sf::Thread(&ResourceManager::readPackages, app->getResourceManager());
    _work->launch();
}

void GameStateLoading::clean() {
    delete _work;
}

void GameStateLoading::pause() {
    //Not need in this state
}

void GameStateLoading::resume() {
    sf::Vector2u size = _app->getWindow()->getSize();
    _width = size.x;
    _height = size.y;
}

void GameStateLoading::event(sf::Event &event) {
    if(event.type == sf::Event::Resized) {
        _width = event.size.width;
        _height = event.size.height;
        _progress.setPosition(0, _height - STATE_LOADING_PROGRESS_HEIGHT);
        _bg.setSize(sf::Vector2f(_width, _height));
    }
}

void GameStateLoading::update(float time) {
    float progress = _app->getResourceManager()->getProgress();
    sf::Vector2f size = _progress.getSize();
    size.x = progress * _width;
    _progress.setSize(size);

    if(progress >= 100)
    {
        LOG("Done loading scene!");
        //if(!Theme::loadFont(*_app->getResourceManager()->getStream("/fonts/bikinis.ttf")))
        //    LOG("Font not loaded!");
        //sf::InputStream* stream0 = _app->getResourceManager()->getStream("/imgs/ui/controls.png");
        //sf::InputStream* stream1 = _app->getResourceManager()->getStream("/layouts/controls.layout");
        //if(!Theme::loadSpriteSheet(*stream0, *stream1))
        //    LOG("UI Sprite not loaded!");
        //delete stream0;
        //delete stream1;
        //stream0 = _app->getResourceManager()->getStream("/styles/controls.styles");
        //if(!Theme::loadStyles(*stream0))
        //    LOG("UI Style not loaded!");
        //delete stream0;
        Theme::init(_app);
        _app->getStateManager()->open(new GameStateMenu());
    }
}

void GameStateLoading::draw(sf::RenderWindow* render) {
    render->draw(_bg);
    render->draw(_progress);
}
