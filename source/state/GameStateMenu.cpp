//
// Created by symbx on 21.03.17.
//

#include "../../include/state/GameStateMenu.h"
#include "../../include/utils/Logger.h"
#include "../../include/state/GameStatePlaying.h"

GameStateMenu::GameStateMenu() :
        _vertex(sf::TrianglesStrip, 4),
        _bg() {

}

void GameStateMenu::init(Application* app) {
    _app = app;

    sf::InputStream* stream = app->getResourceManager()->getStream("/imgs/bg.png");
    _bg.loadFromStream(*stream);
    delete stream;

    sf::Vector2u size = _bg.getSize();
    sf::Vector2u win = app->getWindow()->getSize();

    _vertex[0].position = sf::Vector2f(0, 0);
    _vertex[1].position = sf::Vector2f(win.x, 0);
    _vertex[2].position = sf::Vector2f(0, win.y);
    _vertex[3].position = sf::Vector2f(win.x, win.y);

    _vertex[0].texCoords = sf::Vector2f(0, 0);
    _vertex[1].texCoords = sf::Vector2f(size.x, 0);
    _vertex[2].texCoords = sf::Vector2f(0, size.y);
    _vertex[3].texCoords = sf::Vector2f(size.x, size.y);

    _testBigBtn1 = new BigButton(100, 50, 200, std::wstring(L"TEST Button 1"));
    _testBigBtn2 = new BigButton(100, 98, 200, std::wstring(L"ТЕСТ Кнопка 2"));
    _progressBar = new BigProgressBar(100, 146, 0.5);
    _progress = 0.5;
    _testBtn = new Button(400, 50, 20, 20, std::wstring(L"@"));
    _testBigBtn1->focus(true);
}

void GameStateMenu::clean() {
    delete _testBigBtn1;
    delete _testBigBtn2;
    delete _progressBar;
    delete _testBtn;
}

void GameStateMenu::pause() {
    //Not need in this state
}

void GameStateMenu::resume() {
    sf::Vector2u win = _app->getWindow()->getSize();
    _vertex[1].position = sf::Vector2f(win.x, 0);
    _vertex[2].position = sf::Vector2f(0, win.y);
    _vertex[3].position = sf::Vector2f(win.x, win.y);
}

void GameStateMenu::event(sf::Event &event) {
    if(event.type == sf::Event::Resized) {
        _vertex[1].position = sf::Vector2f(event.size.width, 0);
        _vertex[2].position = sf::Vector2f(0, event.size.height);
        _vertex[3].position = sf::Vector2f(event.size.width, event.size.height);
    }
    if(event.type == sf::Event::KeyPressed) {
        if(event.key.code == sf::Keyboard::Escape)
            _app->quit();
    }
    if(_testBigBtn1->event(event))
        return;
    if(_testBigBtn2->event(event))
        return;
    if(_testBtn->event(event))
        return;
}

void GameStateMenu::update(float time) {
    if(_testBigBtn1->isAction()) {
        _progress -= 0.05;
        _progressBar->setValue(_progress);
        _testBigBtn2->focus(false);
        _testBtn->focus(false);
    }
    if(_testBigBtn2->isAction()) {
        _progress += 0.05;
        _progressBar->setValue(_progress);
        _testBigBtn1->focus(false);
        _testBtn->focus(false);
    }
    if(_testBigBtn1->isFocus()) {
        sf::Int8 tmp = _testBigBtn1->isMoveFocus();
        if(tmp < 0) {
            _testBigBtn1->focus(false);
            _testBtn->focus(true);
        }
        if(tmp > 0) {
            _testBigBtn1->focus(false);
            _testBigBtn2->focus(true);
        }
    }
    if(_testBigBtn2->isFocus()) {
        sf::Int8 tmp = _testBigBtn2->isMoveFocus();
        if(tmp < 0) {
            _testBigBtn2->focus(false);
            _testBigBtn1->focus(true);
        }
        if(tmp > 0) {
            _testBigBtn2->focus(false);
            _testBtn->focus(true);
        }
    }
    if(_testBtn->isFocus()) {
        sf::Int8 tmp = _testBtn->isMoveFocus();
        if(tmp < 0) {
            _testBtn->focus(false);
            _testBigBtn2->focus(true);
        }
        if(tmp > 0) {
            _testBtn->focus(false);
            _testBigBtn1->focus(true);
        }
    }
    if(_testBtn->isAction()) {
        LOG("btn pressed");
        _testBigBtn1->focus(false);
        _testBigBtn2->focus(false);
        _app->getStateManager()->open(new GameStatePlaying());
    }
}

void GameStateMenu::draw(sf::RenderWindow* render) {
    render->draw(_vertex, &_bg);
    render->draw(*_testBigBtn1);
    render->draw(*_testBigBtn2);
    render->draw(*_progressBar);
    render->draw(*_testBtn);
}
