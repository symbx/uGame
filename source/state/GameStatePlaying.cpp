//
// Created by symbx on 23.03.17.
//

#include "../../include/state/GameStatePlaying.h"

GameStatePlaying::GameStatePlaying() {

}

void GameStatePlaying::init(Application* app) {
    _app = app;
    _chunk = new Chunk(app, 0, 0, 0, 0);
    _chunk->loadTestEntity();
}

void GameStatePlaying::clean() {
    delete _chunk;
}

void GameStatePlaying::pause() {

}

void GameStatePlaying::resume() {

}

void GameStatePlaying::event(sf::Event& event) {
    if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
        _app->getStateManager()->close();
    if(_chunk != NULL)
        _chunk->event(event);
}

void GameStatePlaying::update(float time) {
    _chunk->update(time);
}

void GameStatePlaying::draw(sf::RenderWindow* render) {
    render->draw(*_chunk);
}
