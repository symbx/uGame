//
// Created by symbx on 21.03.17.
//

#include "../../include/core/GameStateManager.h"
#include "../../include/core/Application.h"
#include "../../include/state/GameStateLoading.h"

GameStateManager::GameStateManager(Application* app) {
    _app = app;
}

GameStateManager::~GameStateManager() {
    while(!_states.empty())
    {
        _states.back()->clean();
        _states.pop_back();
    }
}

void GameStateManager::init() {
    open(new GameStateLoading());
}

void GameStateManager::open(GameState* state) {
    if(!_states.empty())
        _states.back()->pause();
    state->init(_app);
    _states.push_back(state);
}

void GameStateManager::close() {
    if(!_states.empty()) {
        _states.back()->clean();
        _states.pop_back();
    }
    if(_states.empty()) {
        _app->quit();
    }else{
        _states.back()->resume();
    }
}

GameState* GameStateManager::current() {
    if(_states.empty())
        return NULL;
    return _states.back();
}

void GameStateManager::event(sf::Event &event) {
    if(!_states.empty())
        _states.back()->event(event);
}

void GameStateManager::update(float time) {
    if(!_states.empty())
        _states.back()->update(time);
}

void GameStateManager::draw(sf::RenderWindow* render) {
    if(!_states.empty())
        _states.back()->draw(render);
}
