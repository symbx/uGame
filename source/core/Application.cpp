//
// Created by symbx on 21.03.17.
//

#include <SFML/Window/Event.hpp>
#include "../../include/core/Application.h"
#include "../../include/utils/Logger.h"
#include "../../include/Constants.h"

#ifdef DEBUG
Application::Application(const std::string &appDir) :
        _clock(),
        _fpsText(),
        _fpsFont() {
#else
Application::Application(const std::string &appDir) :
        _clock() {
#endif
    _running = false;
    _cfg = new Config(appDir);
    sf::VideoMode mode = sf::VideoMode::getDesktopMode();
    int width = WINDOW_WIDTH;
    int height = WINDOW_HEIGHT;
    if(width > mode.width)
        width = mode.width;
    if(height > mode.height)
        height = mode.height;
    _window = new sf::RenderWindow(sf::VideoMode(width, height), WINDOW_NAME, sf::Style::Default/*sf::Style::Titlebar | sf::Style::Close*/);
#if FRAME_RATE_FIX
    _window->setFramerateLimit(FRAME_RATE_VAL);
#endif
    _resources = new ResourceManager(appDir);
    _states = new GameStateManager(this);
}

Application::~Application() {
    delete _cfg;
    delete _resources;
}

void Application::_loop() {
    _running = true;

    _states->init();

    LOG("GO!!!");
#ifdef DEBUG
    _fpsText.setPosition(10, 10);
    _fpsText.setCharacterSize(20);
    _fpsText.setFillColor(sf::Color::White);
    _fpsFont.loadFromFile("segoeui.ttf");
    _fpsText.setFont(_fpsFont);
    _fpsText.setString("FPS: 0");
#endif

    while(_window->isOpen())
    {
        sf::Event event;
        while(_window->pollEvent(event))
        {
            _states->event(event);
            if(event.type == sf::Event::Closed)
                _window->close();
            if(event.type == sf::Event::Resized) {
                sf::Vector2u size = sf::Vector2u(event.size.width, event.size.height);
                bool min = false;
                if(size.x < WINDOW_MIN_WIDTH) {
                    size.x = WINDOW_MIN_WIDTH;
                    min = true;
                }
                if(size.y < WINDOW_MIN_HEIGHT) {
                    size.y = WINDOW_MIN_HEIGHT;
                    min = true;
                }
                if(min)
                    _window->setSize(size);
                else
                    _window->setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
            }
        }
        float time = _clock.restart().asSeconds();
        _states->update(time);

        _window->clear(sf::Color::Black);

        _states->draw(_window);
#ifdef  DEBUG
        _calcFPS(time);
        _window->draw(_fpsText);
#endif

        _window->display();
    }
}

void Application::run() {
    if(_running)
        return;
    _loop();
}

void Application::quit() {
    _window->close();
}

ResourceManager* Application::getResourceManager() {
    return _resources;
}

GameStateManager* Application::getStateManager() {
    return _states;
}

sf::RenderWindow* Application::getWindow() {
    return _window;
}

#ifdef DEBUG
void Application::_calcFPS(float time) {
    float fps = 1.f / time;
    _fpsText.setString("FPS: "+std::to_string(fps));
}
#endif