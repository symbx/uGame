//
// Created by symbx on 21.03.17.
//

#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#ifdef DEBUG
#include <SFML/System/FileInputStream.hpp>
#else
#include "../../PackageStream.h"
#endif
#include "../../include/core/ResourceManager.h"
#include "../../include/utils/Logger.h"

ResourceManager::ResourceManager(const std::string &appDir):
        _dir(),
        _packages(),
        _entries() {
#ifdef DEBUG
    _dir = appDir + "res";
    LOG("In debug mode files loading direcly from 'res' directory in workdir instead package.");
    LOG(_dir);
#else
    _dir = appDir + "resources_";
    _complete = false;
    _progress = 0;
    LOG("Init AssetsManager");
#endif
}

ResourceManager::~ResourceManager() {
    for(auto iter = _entries.begin(); iter != _entries.end(); ++iter)
    {
        _entries.erase(iter->first);
    }
}

void ResourceManager::listPackages() {
#ifndef DEBUG
    std::string file;
    LOG("Looking for packages...");
    for(int i = 0; i < 16; i++)
    {
        file = _dir + std::to_string(i) + ".package";
        if(!_fileExists(file))
            break;
        LOG("Found " + file);
        _packages.insert(_packages.begin(), file);
    }
#endif
}

void ResourceManager::readPackages() {
#ifndef DEBUG
    if(_complete)
        return;
    if(_packages.empty())
        listPackages();
    if(_packages.empty())
        return;
    _progressPerFile = 100.0f/_packages.size();
    while(!_packages.empty())
    {
        _readPackage(_packages.back());
        _packages.pop_back();
        _progress += _progressPerFile;
    }
    LOG("Complete");
#endif
    _progress = 100;
    _complete = true;
}

float ResourceManager::getProgress() {
    if(_progress > 100)
        _progress = 100;
    return _progress;
}

sf::InputStream* ResourceManager::getStream(const std::string &f) {
#ifdef DEBUG
    std::string file = _dir + f;
    sf::FileInputStream* stream = new sf::FileInputStream();
    stream->open(file);
    return stream;
#else
    struct ResourceEntry entry = _entries[f];
    return new PackageStream(entry.package, entry.offset, entry.size);
#endif
}

sf::InputStream* ResourceManager::getStream(const char *f) {
    std::string s(f);
    return getStream(s);
}

bool ResourceManager::_fileExists(const std::string &f) {
#if defined(_WIN32)
    struct _stat buff;
    return (_stat(f.c_str(), &buff) == 0);
#else
    struct stat buff;
    return (stat(f.c_str(), &buff) == 0);
#endif
}

void ResourceManager::_readPackage(const std::string &f) {
    LOG("Reading " + f);
    std::ifstream input(f, std::ifstream::binary);

    char head[4];
    input.read(&head[0], 4);
    if(head[0] != 'G' || head[1] != 'R' || head[2] != 'P' || head[3] != 'F') //Game Resources Package File
    {
        LOG("Incorrect header, skip file");
        return;
    }

    sf::Int32 count;
    input.read((char*)&count, sizeof(sf::Int32));

    char nameLen;
    char* buffer = new char[128];
    std::string name;

    for(int i = 0; i < count; i++)
    {
        struct ResourceEntry entry;
        input.read(&nameLen, 1);
        memset(buffer, 0, 128);
        input.read(buffer, (int)nameLen);
        name = std::string(buffer, (int)nameLen);
        entry.package = f;
        input.read((char*)&entry.offset, sizeof(sf::Int64));
        input.read((char*)&entry.size, sizeof(sf::Int64));
        _entries[name] = entry;
    }
    LOG("Done, package contains " + std::to_string(count) + " entries");

    delete[] buffer;
}

void ResourceManager::reload() {
    for(auto iter = _entries.begin(); iter != _entries.end(); ++iter)
    {
        _entries.erase(iter->first);
    }
    _complete = false;
    _progress = 0;
    readPackages();
}
