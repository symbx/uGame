//
// Created by symbx on 21.03.17.
//

#include <fstream>
#include "../../include/core/Config.h"

Config::Config(const std::string &appDir) {
    _cfg = appDir + "game.cfg";

    _map.emplace("volume", "32");

    std::ifstream file(_cfg);
    if(!file.is_open())
        return;

    std::string paramName;
    std::string paramValue;

    while(file >> paramName >> paramValue)
    {
        _map[paramName] = paramValue;
    }

    file.close();
}

Config::~Config() {

}

std::string Config::get(std::string &k) {
    if(_map.count(k) > 0)
        return _map[k];
    return NULL;
}

void Config::set(std::string &k, std::string &v) {
    _map[k] = v;
}

void Config::save() {
    std::ofstream file(_cfg);
    for(auto iter = _map.begin(); iter != _map.end(); ++iter)
        file << iter->first << " " << iter->second << std::endl;
    file.flush();
    file.close();
}
