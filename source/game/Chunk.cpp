//
// Created by symbx on 23.03.17.
//

#include <cstring>
#include "../../include/game/Chunk.h"
#include "../../include/utils/Logger.h"
#include "../../include/game/EntityPlayer.h"
#include "../../include/game/AStar.h"

Chunk::Chunk(Application* app, int world, int layer, int x, int y) :
    _images(),
    _tiles(),
    _objects(),
    _vertex(),
    _view(),
    _cursor() {
    _app = app;
    for (int i = 0; i < MAP_TILES_COUNT; ++i) {
        _objectMap[i] = NULL;
    }
    load(world, layer, x, y);
    sf::Vector2u sz = app->getWindow()->getSize();
    const sf::FloatRect rect = _calcView(sz.x, sz.y);
    _view.reset(rect);
    //_view.setViewport(sf::FloatRect(0, 0, 1, 0.88));
    _view.setViewport(sf::FloatRect(0, 0, 1, 1));
    ResourceManager* res = app->getResourceManager();
    sf::InputStream* str = res->getStream("/imgs/ui/cursor-go.png");
    _cursorTex[0].loadFromStream(*str);
    delete str;
    str = res->getStream("/imgs/ui/cursor-no.png");
    _cursorTex[1].loadFromStream(*str);
    delete str;
    str = res->getStream("/imgs/ui/cursor-use.png");
    _cursorTex[2].loadFromStream(*str);
    delete str;
    str = res->getStream("/imgs/ui/cursor-say.png");
    _cursorTex[3].loadFromStream(*str);
    delete str;
    _cursor.setTexture(_cursorTex[0]);
    _cursor.setPosition(0, 0);
}

void Chunk::loadTestEntity() {
    EntityPlayer* player = new EntityPlayer(_app->getResourceManager(), 5, EntityPlayer::Male, EntityPlayer::Elf, EntityPlayer::Mage);
    player->setPos(3, 8);
    _entities.insert(std::pair<sf::Uint32, Entity*>(player->getID(), (Entity*) player));
    _entitiesMap[8*MAP_SIZE+3].insert((Entity*) player);
    player->setMap(_entitiesMap);
}

void Chunk::update(float time) {
    for (int i = 0; i < MAP_LAYER_TILES; ++i) {
        if(_tilesMap[i] != NULL && _tilesMap[i]->isAnimated() && _tilesMap[i]->update(time)) {
            int p = _tilesMap[i]->getPos()*6;
            _vertex[p] = _tilesMap[i]->getVertex()[0];
            _vertex[p+1] = _tilesMap[i]->getVertex()[1];
            _vertex[p+2] = _tilesMap[i]->getVertex()[2];
            _vertex[p+3] = _tilesMap[i]->getVertex()[1];
            _vertex[p+4] = _tilesMap[i]->getVertex()[2];
            _vertex[p+5] = _tilesMap[i]->getVertex()[3];
        }
    }
    for (int j = 0; j < MAP_TILES_COUNT; ++j) {
        if(_objectMap[j] != NULL)
            _objectMap[j]->update(time);
    }
    for(auto& item : _entities) {
        item.second->update(time);
    }
}

void Chunk::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    sf::View v = target.getView();
    target.setView(_view);
    states.texture = &_images[0];
    target.draw(&_vertex[0], _vertex.size(), sf::Triangles, states);
    for (int j = 0; j < MAP_TILES_COUNT; ++j) {
        if(_objectMap[j] != NULL) {
            target.draw(*_objectMap[j]);
        }
        if(!_entitiesMap[j].empty()) {
            for (auto item = _entitiesMap[j].begin(); item != _entitiesMap[j].end(); ++item)
                target.draw(**item);
        }
    }
    target.draw(_cursor);
    target.setView(v);
}

Chunk::~Chunk() {
    for (int i = 0; i < _tiles.size(); ++i) {
        delete[] _tiles[i]->frames;
        delete _tiles[i];
    }
    for (int j = 0; j < _objects.size(); ++j) {
        for (int i = 0; i < _objects[j]->StatesCount; ++i) {
            for (int k = 0; k < _objects[j]->States[i].FragmentsCount; ++k) {
                delete[] _objects[j]->States[i].Fragments[k].Frames;
            }
            delete[] _objects[j]->States[i].Fragments;
        }
        delete[] _objects[j]->States;
        delete _objects[j];
    }
    for (int i = 0; i < MAP_TILES_COUNT; ++i) {
        delete _tilesMap[i];
        if(_objectMap[i] != NULL)
            delete _objectMap[i];
        if(!_entitiesMap[i].empty())
            _entitiesMap[i].clear();
    }
}

void Chunk::load(int world, int layer, int x, int y) {
    sf::InputStream* mapStream = _app->getResourceManager()->getStream(
            "/terrain/world-"+std::to_string(world)+
            "/layer-"+std::to_string(layer)+
            '/'+std::to_string(x)+'x'+std::to_string(y)+".chunk");

    //Images count
    sf::Uint8 imgsCount;
    mapStream->read(&imgsCount, 1);
    _images.reserve(imgsCount);
    //Buffer for image path
    char* buffer = new char[128];
    memset(buffer, 0, 128);
    for(sf::Uint8 i = 0; i < imgsCount; i++) {
        //Read path
        sf::Uint8 nameLen;
        mapStream->read(&nameLen, 1);
        mapStream->read(buffer, nameLen);
        std::string name;
        name.assign(buffer, nameLen);
        //Load texture
        sf::Texture texture;
        sf::InputStream* textureStream = _app->getResourceManager()->getStream(name);
        texture.loadFromStream(*textureStream);
        delete textureStream;
        _images.push_back(texture);
        //cleanup
        memset(buffer, 0, 128);
    }
    delete[] buffer;

    //Tiles count
    sf::Uint16 tilesCount;
    mapStream->read(&tilesCount, 2);
    _tiles.reserve(tilesCount);
    for(sf::Uint16 i = 0; i < tilesCount; i++) {
        Tile* tile = new Tile();
        mapStream->read(&tile->frameTime, 4); //Time
        mapStream->read(&tile->framesCount, 1); //Count
        tile->frames = new sf::Uint16[tile->framesCount]; //Prepare array
        for (sf::Uint8 j = 0; j < tile->framesCount; ++j) {
            mapStream->read(&tile->frames[j], 2); //Read frames
        }
        _tiles.push_back(tile);
    }

    //TODO: Optimize x,y loop -> one loop (y*size+x)
    //Reserve only for one layer
    _vertex.reserve(MAP_TILES_COUNT*6);
    //Read layer 1
    sf::Uint16 vPos = 0;
    for (sf::Uint8 ty = 0; ty < MAP_SIZE; ++ty) {
        for (sf::Uint8 tx = 0; tx < MAP_SIZE; ++tx) {
            sf::Int16 tileId;
            mapStream->read(&tileId, 2);
            if(tileId >= 0) {
                TileBlock* bl = new TileBlock(_tiles[tileId], tx, ty);
                _tilesMap[ty * MAP_SIZE + tx] = bl;
                bl->setPos(vPos); // TODO: May be bl->setPos(vPos++);
                _vertex.push_back(bl->getVertex()[0]);
                _vertex.push_back(bl->getVertex()[1]);
                _vertex.push_back(bl->getVertex()[2]);
                _vertex.push_back(bl->getVertex()[1]);
                _vertex.push_back(bl->getVertex()[2]);
                _vertex.push_back(bl->getVertex()[3]);
                vPos++;
            } else
                _tilesMap[ty*MAP_SIZE+tx] = NULL;
        }
    }

    //Read layer 2
    for (sf::Uint8 ty = 0; ty < MAP_SIZE; ++ty) {
        for (sf::Uint8 tx = 0; tx < MAP_SIZE; ++tx) {
            sf::Int16 tileId;
            mapStream->read(&tileId, 2);
            if(tileId >= 0) {
                TileBlock* bl = new TileBlock(_tiles[tileId], tx, ty);
                _tilesMap[MAP_TILES_COUNT + (ty * MAP_SIZE + tx)] = bl;
                bl->setPos(vPos); // TODO: May be bl->setPos(vPos++);
                _vertex.push_back(bl->getVertex()[0]);
                _vertex.push_back(bl->getVertex()[1]);
                _vertex.push_back(bl->getVertex()[2]);
                _vertex.push_back(bl->getVertex()[1]);
                _vertex.push_back(bl->getVertex()[2]);
                _vertex.push_back(bl->getVertex()[3]);
                vPos++;
            } else
                _tilesMap[MAP_TILES_COUNT+(ty*MAP_SIZE+tx)] = NULL;
        }
    }
    _vertex.shrink_to_fit();

    // TODO: Read solidity map
    sf::Uint8 tmp;
    for (int m = 0; m < MAP_SIZE; ++m) {
        for (int i = 0; i < MAP_SIZE; ++i) {
            mapStream->read(&tmp, 1);
            _solidMap[m*MAP_SIZE+i] = tmp;
        }
    }

    sf::Uint16 objCount;
    mapStream->read(&objCount, 2);

    _objects.reserve(objCount);
    for (int k = 0; k < objCount; ++k) {
        Object* o = new Object();
        mapStream->read(&o->Image, 1);
        mapStream->read(&o->Width, 1);
        mapStream->read(&o->Height, 1);
        mapStream->read(&o->StatesCount, 1);
        o->States = new ObjectState[o->StatesCount];
        for (int i = 0; i < o->StatesCount; ++i) {
            o->States[i] = ObjectState();
            mapStream->read(&o->States[i].InteractX, 1);
            mapStream->read(&o->States[i].InteractY, 1);
            mapStream->read(&o->States[i].FragmentsCount, 1);
            o->States[i].Fragments = new ObjectFragment[o->States[i].FragmentsCount];
            for (int j = 0; j < o->States[i].FragmentsCount; ++j) {
                o->States[i].Fragments[j] = ObjectFragment();
                mapStream->read(&o->States[i].Fragments[j].FrameTime, 4);
                mapStream->read(&o->States[i].Fragments[j].PosX, 1);
                mapStream->read(&o->States[i].Fragments[j].PosY, 1);
                mapStream->read(&o->States[i].Fragments[j].Width, 1);
                mapStream->read(&o->States[i].Fragments[j].Height, 1);
                mapStream->read(&o->States[i].Fragments[j].FramesCount, 1);
                o->States[i].Fragments[j].Frames = new sf::Uint16[o->States[i].Fragments[j].FramesCount];
                for (int l = 0; l < o->States[i].Fragments[j].FramesCount; ++l) {
                    mapStream->read(&o->States[i].Fragments[j].Frames[l], 2);
                }
            }
        }
        _objects.push_back(o);
    }
    LOG("Loaded objects: "+std::to_string(_objects.size()));

    for (sf::Uint8 m = 0; m < MAP_SIZE; ++m) {
        for (sf::Uint8 i = 0; i < MAP_SIZE; ++i) {
            sf::Int16 id;
            mapStream->read(&id, 2);
            if(id >= 0) {
                ObjectBlock* ob = new ObjectBlock(_objects[id], &_images[_objects[id]->Image], i, m);
                _objectMap[m*MAP_SIZE+i] = ob;
            }
        }
    }

    delete mapStream;
}

sf::FloatRect Chunk::_calcView(int w, int h) {
    // TODO: Move 0.88 to constants header
    sf::FloatRect out(0, 0, 1, 1);
    if(w > 1024 || h > 1024) {
        if(w > h) {
            out.width = 1024;
            out.height = (h/(float)w)*1024;
            //out.height = ((h*0.88f)/(float)w)*1024;
        } else {
            out.height = h;
            out.width = (w/(float)h)*1024;
            //out.height = h*0.88f;
            //out.width = (w/(h*0.88f))*1024;
        }
        return out;
    }
    out.width = w;
    out.height = h;
    //out.height = h*0.88f;
    return out;
}

void Chunk::event(sf::Event& event) {
    if(event.type == sf::Event::Resized) {
        const sf::FloatRect rect = _calcView(event.size.width, event.size.height);
        _view.reset(rect);
    }
    if(event.type == sf::Event::KeyPressed) {
        sf::Vector2f pos = _cursor.getPosition();
        sf::Uint8 x = pos.x / 32;
        sf::Uint8 y = pos.y / 32;
        if(event.key.code == sf::Keyboard::Left && x > 0) {
            x -= 1;
            _cursor.setPosition(x*32, y*32);
        }
        if(event.key.code == sf::Keyboard::Right && x < MAP_SIZE-1) {
            x += 1;
            _cursor.setPosition(x*32, y*32);
        }
        if(event.key.code == sf::Keyboard::Up && y > 0) {
            y -= 1;
            _cursor.setPosition(x*32, y*32);
        }
        if(event.key.code == sf::Keyboard::Down && y < MAP_SIZE-1) {
            y += 1;
            _cursor.setPosition(x*32, y*32);
        }
        if(event.key.code == sf::Keyboard::Return) {
            EntityPlayer* player = (EntityPlayer*) _entities.at(5);
            sf::Vector2u uPos = player->getPos();
            if(x != uPos.x || y != uPos.y) {
                std::clock_t start = std::clock();
                std::vector<short> path = AStar::GetPath(uPos.x, uPos.y, x, y, _solidMap);
                std::clock_t end = std::clock() - start;
                LOG("Finish path with "+std::to_string((end/CLOCKS_PER_SEC)*1000.0)+" ms with "+std::to_string(path.size())+" steps");
                LOG("Start pos "+std::to_string(uPos.x)+'x'+std::to_string(uPos.y));
                if(path.size() == 0)
                    LOG("Path do not exists!");
                else {
                    player->resetPath();
                    for (int i = 0; i < path.size(); ++i) {
                        sf::Uint8 xn = path[i] % MAP_SIZE;
                        sf::Uint8 yn = path[i] / MAP_SIZE;
                        LOG("Go to " + std::to_string(xn) + 'x' + std::to_string(yn));
                        player->goTo(xn, yn);
                    }
                }
            }
        }
    }
    if(event.type == sf::Event::MouseButtonPressed) {
        sf::Vector2f cPos = _app->getWindow()->mapPixelToCoords(sf::Vector2i(event.mouseButton.x, event.mouseButton.y), _view);
        sf::Uint8 cx = cPos.x/32;
        sf::Uint8 cy = cPos.y/32;
        LOG("Press at " + std::to_string(cx) + 'x' + std::to_string(cy));
        _cursor.setPosition(cx*32, cy*32);
        if(_solidMap[cy*MAP_SIZE+cx]) {
            _cursor.setTexture(_cursorTex[1]);
            return;
        }
        _cursor.setTexture(_cursorTex[0]);
        EntityPlayer* player = (EntityPlayer*) _entities.at(5);
        sf::Vector2u uPos = player->getPos();
        std::clock_t start = std::clock();
        std::vector<short> path = AStar::GetPath(uPos.x, uPos.y, cx, cy, _solidMap);
        std::clock_t end = std::clock() - start;
        LOG("Finish path with "+std::to_string((end/CLOCKS_PER_SEC)*1000.0)+" ms with "+std::to_string(path.size())+" steps");
        LOG("Start pos "+std::to_string(uPos.x)+'x'+std::to_string(uPos.y));
        if(path.size() == 0)
            LOG("Path do not exists!");
        else {
            player->resetPath();
            for (int i = 0; i < path.size(); ++i) {
                sf::Uint8 x = path[i] % MAP_SIZE;
                sf::Uint8 y = path[i] / MAP_SIZE;
                LOG("Go to " + std::to_string(x) + 'x' + std::to_string(y));
                player->goTo(x, y);
            }
        }
    }
}
