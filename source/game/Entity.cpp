//
// Created by symbx on 28.03.17.
//

#include "../../include/game/Entity.h"
#include "../../include/Constants.h"

Entity::Entity(ResourceManager* res, sf::Uint32 id) :
    _transform(),
    _path() {
    _ID = id;
    _res = res;
    _transform = sf::Transform(1, 0, 0,
                               0, 1, 0,
                               0, 0, 1);
    _entitiesMap = NULL;
}

sf::Uint32 Entity::getID() {
    return _ID;
}

void Entity::setDirection(Entity::Direction d) {
    if(_dir != d)
        _dir = d;
}

Entity::Direction Entity::getDirection() {
    return _dir;
}

void Entity::setPos(sf::Uint8 x, sf::Uint8 y) {
    if(_entitiesMap != NULL) {
        _entitiesMap[_posY * 32 + _posX].erase(this);
        _entitiesMap[y * 32 + x].insert(this);
    }
    _posX = x;
    _posY = y;
    _transform = sf::Transform(1, 0, x*32,
                               0, 1, y*32,
                               0, 0, 1);
}

sf::Vector2u Entity::getPos() {
    return sf::Vector2u(_posX, _posY);
}

void Entity::setMap(std::set<Entity*>* entitiesMap) {
    _entitiesMap = entitiesMap;
}

void Entity::goTo(sf::Uint8 x, sf::Uint8 y) {
    short p = y*MAP_SIZE+x;
    if(_path.empty() || _path.back() != p)
        _path.push(p);
}

void Entity::resetPath() {
    std::queue<short> tmp;
    _path.swap(tmp);
}
