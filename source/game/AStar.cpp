//
// Created by symbx on 04.05.17.
//

#include <queue>
#include <algorithm>
#include "../../include/game/AStar.h"
#include "../../include/Constants.h"

std::vector<short> AStar::GetPath(sf::Int8 sX, sf::Int8 sY, sf::Int8 eX, sf::Int8 eY, bool* map) {
    std::priority_queue<PathNode> openList;
    std::priority_queue<PathNode> closeList;
    std::priority_queue<PathNode> tmpList;
    float openMap[MAP_TILES_COUNT];
    bool closeMap[MAP_TILES_COUNT];
    sf::Int8 dirMap[MAP_TILES_COUNT];
    for (int i = 0; i < MAP_TILES_COUNT; ++i) {
        openMap[i] = 0;
        closeMap[i] = false;
        dirMap[i] = -1;
    }

    PathNode sNode;
    sNode.x = sX;
    sNode.y = sY;
    sNode.start = 0;
    sNode.estimate(eX, eY);
    sNode.cost = sNode.start + sNode.finish;
    openList.push(sNode);

    while(!openList.empty()) {
        PathNode node = openList.top();
        openList.pop();

        if(node.x == eX && node.y == eY) {
            sf::Int8 x = eX;
            sf::Int8 y = eY;
            short pos = y*MAP_SIZE+x;
            if (dirMap[pos] == -1)
            {
                return {}; //Path not exists
            }
            std::vector<short> res;
            while(!(x == sX && y == sY)) {
                //char c;
                switch(dirMap[pos]) {
                    case 0:
                        //c = '<';
                        x += 1;
                        break;
                    case 1:
                        //c = 'A';
                        y += 1;
                        break;
                    case 2:
                        //c = '>';
                        x -= 1;
                        break;
                    case 3:
                        //c = 'V';
                        y -= 1;
                        break;
                }
                res.push_back(pos);
                pos = y*MAP_SIZE+x;
            }
            while(!openList.empty()) {
                openList.pop();
            }
            //todo: reverse vector
            std::reverse(res.begin(), res.end());
            return res; //Reconstruct path
        }

        closeMap[node.y*MAP_SIZE+node.x] = true;
        for (sf::Int8 i = 0; i < 4; ++i) {
            sf::Int8 x = node.x;
            sf::Int8 y = node.y;
            switch (i) {
                case 0:
                    x -= 1;
                    break;
                case 1:
                    y -= 1;
                    break;
                case 2:
                    x += 1;
                    break;
                case 3:
                    y += 1;
                    break;
            }

            int pos = y*MAP_SIZE+x;
            if(x < 0 || y < 0 || x >= MAP_SIZE || y >= MAP_SIZE || map[pos] || closeMap[pos]) {
                continue;
            }

            PathNode* nei = new PathNode();
            nei->x = x;
            nei->y = y;
            nei->start = node.start + 5;
            nei->estimate(eX, eY);

            if(openMap[pos] == 0) {
                openMap[pos] = nei->cost;
                dirMap[pos] = i;
                openList.push(*nei);
            } else if(openMap[pos]>nei->cost) {
                openMap[pos] = nei->cost;
                dirMap[pos] = i;
                while(!(openList.top().x == nei->x && openList.top().y == nei->y)) {
                    tmpList.push(openList.top());
                    openList.pop();
                }
                openList.pop();
                openList.push(*nei);
                while(!tmpList.empty()) {
                    openList.push(tmpList.top());
                    tmpList.pop();
                }
            }
            delete nei;
        }
    }
    while(!openList.empty()) {
        openList.pop();
    }
    return {};
}
