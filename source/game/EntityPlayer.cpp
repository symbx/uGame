//
// Created by symbx on 28.03.17.
//

#include "../../include/game/EntityPlayer.h"
#include "../../include/Constants.h"
#include "../../include/utils/Logger.h"


EntityPlayer::EntityPlayer(ResourceManager* res, sf::Uint32 id, Gender g, EntityPlayer::Fraction f, EntityPlayer::Class c) : Entity(res, id),
    _body(sf::TrianglesStrip, 4){
    _gender = g;
    _fraction = f;
    _class = c;
    _loadTextures();
    _body[0].position = sf::Vector2f(-12, -32);
    _body[1].position = sf::Vector2f(-12, 24);
    _body[2].position = sf::Vector2f(44, -32);
    _body[3].position = sf::Vector2f(44, 24);

    _body[0].texCoords = sf::Vector2f(0, 640);
    _body[1].texCoords = sf::Vector2f(0, 704);
    _body[2].texCoords = sf::Vector2f(64, 640);
    _body[3].texCoords = sf::Vector2f(64, 704);
    _action = None;
    _frame = 0;
    _maxFrames = 1;
    _frameTime = 0;
}

void EntityPlayer::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    states.transform *= _transform;
    states.texture = &_bodyTexture;
    target.draw(_body, states);
}

void EntityPlayer::setAction(EntityPlayer::Action a) {
    if(_action == a)
        return;
    _action = a;
    _frame = 0;
    _frameTime = 0;
    switch (_action) {
        case None:
        default:
            _maxFrames = 1;
            _actionY = 512;
            break;
        case Walk:
            _maxFrames = 9;
            _actionY = 512;
            break;
        case AttackSpear:
            _maxFrames = 8;
            _actionY = 256;
            break;
        case AttackSword1H:
            _maxFrames = 6;
            _actionY = 768;
            break;
        case AttackBow:
            _maxFrames = 13;
            _actionY = 1024;
            break;
        case CastShield:
            _maxFrames = 7;
            _actionY = 0;
            break;
        case Die:
            _maxFrames = 6;
            _actionY = 1280;
            break;
    }
    LOG("Set action");
    _updateFrame();
}

void EntityPlayer::_loadTextures() {
    std::string bodyPath("/imgs/entities/player/body/");
    if(_gender == Female)
        bodyPath += "fe";
    bodyPath += "male-" + std::to_string(_fraction) + ".png";
    sf::InputStream* _stream = _res->getStream(bodyPath);
    _bodyTexture.loadFromStream(*_stream);
    delete _stream;
}

void EntityPlayer::update(float time) {
    if(!_path.empty()) {
        setAction(Walk);
        short next = _path.front();
        sf::Uint8 x = (sf::Uint8) (next % MAP_SIZE);
        sf::Uint8 y = (sf::Uint8) (next / MAP_SIZE);
        float nx = x*MAP_SIZE;
        float ny = y*MAP_SIZE;
        float rx = _posX*MAP_SIZE;
        float ry = _posY*MAP_SIZE;
        float shift = PLAYER_SPEED*time;
        if(rx+_shiftX != nx) {
            if(_posX < x) {
                setDirection(RIGHT);
                shift = (rx + _shiftX + shift > nx) ? nx - rx : shift;
            }else
            if(_posX > x) {
                setDirection(LEFT);
                shift = (rx + _shiftX + shift < nx) ? nx - rx : -shift;
            }else
            if(_posX == x) {
                shift = std::abs(_shiftX) > shift ? shift : std::abs(_shiftX);
                shift = (_shiftX > 0) ? -shift : shift;
            }
            _shiftX += shift;
            _transform.translate(shift, 0);
            if(std::abs(_shiftX) > 16  && _posX != x) {
                _shiftX *= -1;
                setPos(x, _posY);
                _transform.translate(_shiftX, 0);
            }else
            if(_shiftX == 0 && _posX == x) {
                _path.pop();
            }
        }else
        if(ry+_shiftY != ny) {
            if(_posY < y) {
                setDirection(DOWN);
                shift = (ry + _shiftY + shift > ny) ? ny - ry : shift;
            }else
            if(_posY > y) {
                setDirection(UP);
                shift = (ry + _shiftY + shift < ny) ? ny - ry : -shift;
            }else
            if(_posY == y) {
                shift = std::abs(_shiftY) > shift ? shift : std::abs(_shiftY);
                shift = (_shiftY > 0) ? -shift : shift;
            }
            _shiftY += shift;
            _transform.translate(0, shift);
            if(std::abs(_shiftY) > 16 && _posY != y) {
                _shiftY *= -1;
                setPos(_posX, y);
                _transform.translate(0, _shiftY);
            }else
            if(_shiftY == 0 && _posY == y) {
                _path.pop();
            }
        }
        //LOG("Now pos "+std::to_string((_posX*32)+_shiftX)+'x'+std::to_string((_posY*32)+_shiftY));
    }else{
        setAction(None);
    }
    if(_maxFrames == 1)
        return;
    _frameTime += time;
    if(_frameTime > 0.1f) {
        _frame++;
        _frameTime = 0; //or _frameTime -= 0.1f;
        if (_frame >= _maxFrames)
            _frame = 1;
        _updateFrame();
    }
}

/*void EntityPlayer::goTo(std::set<Entity*>* entitiesMap, sf::Uint8 x, sf::Uint8 y) {
    _entitiesMap = entitiesMap;
    short p = y*MAP_SIZE+x;
    if(_path.empty() || _path.back() != p)
        _path.push(p);
}*/

void EntityPlayer::setDirection(Entity::Direction d) {
    if(_dir == d)
        return;
    _dir = d;
    switch (_dir) {
        case UP:
            _dirY = 0;
            break;
        case LEFT:
            _dirY = 64;
            break;
        case DOWN:
            _dirY = 128;
            break;
        case RIGHT:
            _dirY = 192;
            break;
    }
    _updateFrame();
}

void EntityPlayer::_updateFrame() {
    int yPos = _actionY + _dirY;
    int xPos = _frame * 64;
    _body[0].texCoords.y = yPos;
    _body[1].texCoords.y = yPos+64;
    _body[2].texCoords.y = yPos;
    _body[3].texCoords.y = yPos+64;
    _body[0].texCoords.x = xPos;
    _body[1].texCoords.x = xPos;
    _body[2].texCoords.x = xPos+64;
    _body[3].texCoords.x = xPos+64;
}
